﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Plus.GFile;
using Plus.GIO;

namespace Plus.Functional
{
    public class GenerateClassFiles : Singleton<GenerateClassFiles>
    {
        #region 常量

        /// <summary>
        /// Tab
        /// 键盘制表定位键
        /// </summary>
        private const string TabulatorStr = "\t{0}";
        /// <summary>
        /// 换行
        /// </summary>
        private const string Wrap = "{0}\n";

        private const string System = "System";
        private const string SystemCG = System + ".Collections.Generic";
        private const string SystemL = System + ".Linq";
        private const string SystemT = System + ".Text";
        private const string SystemTT = System + ".Threading.Tasks";
        private const string UnityEng = "UnityEngine";


        private const string Data = "Data";
        private const string DataB = "CSVDataBase";


        /// <summary>
        /// 命名空间
        /// </summary>
        private const string namesp = "namespace {0}";
        /// <summary>
        /// 类
        /// </summary>
        private const string ClassStr = "public class {0} : {1}";
        /// <summary>
        /// 构造
        /// </summary>
        private const string StructureStr = "public {0}()";

        /// <summary>
        /// 属性
        /// </summary>
        private const string AttributesStr = "public {0} {1} { get => {2}; set => {2} = value; }";

        /// <summary>
        /// OnSet
        /// 方法代码
        /// </summary>
        private const string OnSetHesdStr = "protected override void OnSet(List<string> list)";
        /// <summary>
        /// 编号代码块
        /// </summary>
        private const string SerialNumberCodeStr = "int i = {0};";
        /// <summary>
        /// TByString
        /// 代码块
        /// </summary>
        private const string TByStringCodeStr = "{0} = sP.{1}ByString<{2}>(list[++i]);";

        /// <summary>
        /// OnGet
        /// 方法代码
        /// </summary>
        private const string OnGetHesdStr = "protected override List<string> OnGet(List<string> list)";
        /// <summary>
        /// StringByT
        /// 代码块
        /// </summary>
        private const string StringByTCodeStr = "list.Add(sP.StringBy{0}({1}));";
        /// <summary>
        /// 退出代码块
        /// </summary>
        private const string ReturnListCodeStr = "return list;";

        #endregion

        #region 所使用的工具类

        StringPlus sp_ => StringPlus.Instance;
        #endregion

        #region 字段

        //using
        List<string> us_ = new List<string>();
        //namespace
        string namespace_;
        //class
        string class_;
        //Parent 父类
        string parent_;
        //字段
        List<string> field_ = new List<string>();
        //主键
        List<string> main_key_ = new List<string>();
        //基础字段类型
        List<string> field_Type_ = new List<string>();
        //当前字段类型
        List<string> field_Type_Now_ = new List<string>();
        //是否为储存类型
        List<string> field_Type_Storage_ = new List<string>();
        //注释
        List<string> exegesis_ = new List<string>();
        //路径
        string path_;
        //路径
        string sevePath_;
        //名称
        string name_;
        //初始下表
        int index_;

        public string GetClassName
        {
            get
            {
                return string.Format("{0}CSVData", class_);
            }
        }
        #endregion

        #region 构造

        public GenerateClassFiles()
        {
            InitField();
        }

        private void InitField()
        {
            InitList<string>(ref us_);
            InitList<string>(ref field_);
            InitList<string>(ref field_Type_);
            InitList<string>(ref field_Type_Now_);
            InitList<string>(ref field_Type_Storage_);
            InitList<string>(ref exegesis_);
            namespace_ = default;
            class_ = default;
            parent_ = default;
            path_ = default;
            name_ = default;
            index_ = default;

            us_.Add(System);
            us_.Add(SystemCG);
            us_.Add(SystemL);
            us_.Add(SystemT);
            us_.Add(SystemTT);
            us_.Add(UnityEng);
        }

        #endregion

        #region 基础的方法

        public void CreatCSClass(string path, string name, string sevePath, string name_space = Data, string parent = DataB, int index = 4)
        {
            path_ = path;
            name_ = name;
            sevePath_ = sevePath;
            class_ = name;
            parent_ = parent;
            index_ = index;
            namespace_ = name_space;
            OnInitData();
            //SpliceClass();
            SpliceClassTwo();

            InitField();
        }

        #endregion

        #region 拼接的方法

        private void SpliceClass()
        {
            List<string> strs = new List<string>();

            strs.AddRange(GetUsings());
            strs.AddRange(GetNameSpace());

            string str = default;

            for (int i = 0; i < strs.Count; i++)
            {
                str += string.Format(Wrap, strs[i]);// $"{strs[i]}\n";
            }
            DirectoryPlus.AddDirectory(path_, "Script");
            FileManage.SetCS($"{path_}\\Script\\{name_}", str);

        }
        private void SpliceClassTwo()
        {
            List<string> strs = new List<string>();

            strs.AddRange(GetUsings());
            strs.AddRange(GetNameSpace());

            string str = default;

            for (int i = 0; i < strs.Count; i++)
            {
                str += string.Format(Wrap, strs[i]);// $"{strs[i]}\n";
            }
            DirectoryPlus.AddDirectory(sevePath_);
            FileManage.SetCS($"{sevePath_}\\{GetClassName}", str);
            //UnityEngine.Debug.Log(path_);
            //UnityEngine.Debug.Log($"{path_}\n{str}");

        }

        /// <summary>
        /// 获取命名空间里对应的数据
        /// </summary>
        /// <returns></returns>
        private List<string> GetNameSpace()
        {
            List<string> strs = new List<string>();

            strs.AddRange(SplicingBracesAddVacancy(string.Format(namesp, namespace_), GetClass()));

            return strs;
        }

        /// <summary>
        /// 获取类
        /// </summary>
        /// <returns></returns>
        private List<string> GetClass()
        {
            List<string> strs = new List<string>();
            strs.AddRange(SplicingBracesAddVacancy(string.Format(ClassStr, GetClassName, parent_), GetField(), GetAttributes(), GetMainKey(), GetStructure(), GetFunction()));
            return strs;
        }

        /// <summary>
        /// 获取构造
        /// </summary>
        /// <returns></returns>
        private List<string> GetStructure()
        {
            List<string> strs = new List<string>();

            //strs.AddRange(SplicingBraces($"public {class_}({class_} _this)", $"Set(_this.Get());"));
            strs.AddRange(SplicingBraces(string.Format(StructureStr, GetClassName)));
            return strs;
        }

        /// <summary>
        /// 获取方法
        /// </summary>
        /// <returns></returns>
        private List<string> GetFunction()
        {
            List<string> strs = new List<string>();
            if (field_.Count > 0)
            {
                strs.AddRange(SplicingBraces(OnSetHesdStr, AddVacancy(FunctionSet())));
                strs.AddRange(SplicingBraces(OnGetHesdStr, AddVacancy(FunctionGet())));
            }
            return strs;
        }

        /// <summary>
        /// 编写 Get 方法
        /// </summary>
        /// <returns></returns>
        public List<string> FunctionGet()
        {
            List<string> strs = new List<string>();

            for (int i = 0; i < field_.Count; i++)
            {
                //strs.Add($"list.Add(sP.StringBy{field_Type_Storage_[i]}<{field_Type_[i]}>({field_[i]}));");
                strs.Add(string.Format(StringByTCodeStr, field_Type_Storage_[i], field_[i]));
            }
            strs.Add(ReturnListCodeStr);
            return strs;
        }
        /// <summary>
        /// 编写 Set 方法
        /// </summary>
        /// <returns></returns>
        public List<string> FunctionSet()
        {
            List<string> strs = new List<string>();

            strs.Add(string.Format(SerialNumberCodeStr, index_));
            for (int i = 0; i < field_.Count; i++)
            {
                strs.Add(string.Format(TByStringCodeStr, field_[i], field_Type_Storage_[i], field_Type_[i]));
            }

            return strs;
        }

        /// <summary>
        /// 获取属性
        /// </summary>
        /// <returns></returns>
        private List<string> GetAttributes()
        {
            List<string> strs = new List<string>();
            for (int i = 0; i < field_.Count; i++)
            {
                string str = StringPlus.ToUp(0, 1, field_[i]);
                if (str == field_[i])
                {
                    str += "1";
                }
                strs.Add("/// <summary>");
                strs.Add(string.Format("/// {0}", exegesis_[i]));
                strs.Add("/// </summary>");
                strs.Add(GetAttributesStr(field_Type_Now_[i], str, field_[i]));
            }
            return strs;
        }

        /// <summary>
        /// 获取主键
        /// </summary>
        /// <returns></returns>
        private List<string> GetMainKey()
        {
            List<string> strs = new List<string>();

            strs.AddRange(main_key_);

            return strs;
        }

        /// <summary>
        /// 获取字段
        /// </summary>
        /// <returns></returns>
        private List<string> GetField()
        {
            List<string> strs = new List<string>();
            for (int i = 0; i < field_.Count; i++)
            {
                strs.Add($"//{exegesis_[i]}");
                strs.Add($"private {field_Type_Now_[i]} {field_[i]};");
            }
            return strs;
        }

        /// <summary>
        /// 获取所有的using
        /// </summary>
        /// <returns></returns>
        private List<string> GetUsings()
        {
            List<string> strs = new List<string>();
            for (int i = 0; i < us_.Count; i++)
            {
                strs.Add($"using {us_[i]};");
            }
            return strs;
        }

        /// <summary>
        /// 初始化数据
        /// 读表
        /// 根据表中的内容设置数据
        /// </summary>
        private void OnInitData()
        {
            InitList(ref field_);
            InitList(ref main_key_);
            InitList(ref field_Type_);
            InitList(ref field_Type_Now_);
            InitList(ref field_Type_Storage_);
            InitList(ref exegesis_);

            List<List<string>> vs = new List<List<string>>();
            FileManage.GetCSV(path_, ref vs, DirectoryType.Assets);
            //FileManage.GetCSV($"{path_}\\{name_}", ref vs, DirectoryType.Assets);

            /**
             * 目前是 
             * 
             * List~int
             * 
             * List 为 储存类型 下标为 0
             * int  为 基础类型 下标为 1
             */

            //储存类型的下标
            int index_data_structure = 0;
            //基础类型的下标
            int index_type = 1;

            /**
             * 目前表结构
             * 
             * index , name
             * 下标  ，名称
             * int   , string
             * 
             * index 字段名    下标为 0
             * 下标  字段描述  下标为 1
             * int   字段类型  下标为 2
             */

            //字段名的下标
            int index_field = 0;
            //字段描述的下标
            int index_exegesis = 1;
            //字段类型的下标
            int index_field_type = 2;
            if (vs.Count > index_field_type + 1)
                for (int i = index_ + 1; i < vs[index_field].Count; i++)
                {
                    if (vs[index_field][i] == null || vs[index_field][i] == "")
                    {
                        continue;
                    }
                    string[] field_str_names = vs[index_field][i].Split(' ');

                    string field_str = "";
                    if (field_str_names.Length > 0)
                    {
                        field_str = field_str_names[0];
                    }

                    if (field_str_names.Length > 1)
                    {
                        if (field_str_names[1] == "main_key")
                        {
                            string main_key_str = "";

                            string str = StringPlus.ToUp(0, 1, field_str);
                            if (str == field_str)
                            {
                                str += "1";
                            }

                            if (vs[index_field_type][i] == "int")
                            {
                                main_key_str = string.Format("public override int GetImportantId => {0};", str);
                                if (main_key_.Count > 0)
                                {
                                    main_key_[0] = main_key_str;
                                }
                                else
                                {
                                    main_key_.Add(main_key_str);
                                }
                            }
                            else if (vs[index_field_type][i] == "string")
                            {
                                main_key_str = string.Format("public override string GetImportantName => {0};", str);
                                if (main_key_.Count > 1)
                                {
                                    main_key_[1] = main_key_str;
                                }
                                else
                                {
                                    main_key_.Add(main_key_str);
                                }
                            }
                            //public override int GetImportantId => Section_id;

                        }
                    }

                    field_.Add(field_str);
                    exegesis_.Add(vs[index_exegesis][i]);
                    string[] strs_sp = vs[index_field_type][i].Split('~');

                    if (strs_sp.Length > 1)
                    {
                        if (strs_sp[index_data_structure] == "List")
                        {
                            field_Type_Now_.Add($"List<{strs_sp[index_type]}>");
                        }
                        else if (strs_sp[index_data_structure] == "Dictionary")
                        {
                            field_Type_Now_.Add($"Dictionary<int,List<{strs_sp[index_type]}>>");
                        }
                        else
                        {
                            field_Type_Now_.Add(vs[index_field_type][i]);
                        }
                        field_Type_.Add(strs_sp[index_type]);
                        field_Type_Storage_.Add(strs_sp[index_data_structure]);
                    }
                    else
                    {
                        field_Type_.Add(vs[index_field_type][i]);
                        field_Type_Now_.Add(vs[index_field_type][i]);
                        field_Type_Storage_.Add("T");
                    }
                }

        }

        #endregion

        #region 辅助

        /// <summary>
        /// 初始化
        /// List
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="ts"></param>
        public void InitList<T>(ref List<T> ts)
        {
            if (ts == null)
            {
                ts = new List<T>();
            }
            else
            {
                ts.Clear();
            }
        }


        /// <summary>
        /// 增加空位
        /// </summary>
        /// <param name="strs_l"></param>
        /// <returns></returns>
        public List<string> AddVacancy(List<string> strs_l)
        {
            List<string> strs = new List<string>();

            for (int i = 0; i < strs_l.Count; i++)
            {
                strs.Add(string.Format(TabulatorStr, strs_l[i]));
            }
            return strs;
        }

        /// <summary>
        /// 获得属性
        /// </summary>
        /// <param name="typeStr">类型</param>
        /// <param name="attributesNameStr">属性名</param>
        /// <param name="fieldNameStr">字段名</param>
        public string GetAttributesStr(string typeStr,string attributesNameStr, string fieldNameStr)
        {
            //"public {0} {1} { get => {2}; set => {2} = value; }";
            string nameStr = string.Format("public {0} {1} ", typeStr, attributesNameStr);
            string bodyStr = string.Format(" get => {0}; set => {0} = value; ", fieldNameStr);
            string attributes = string.Concat(nameStr, "{", bodyStr, "}");
            return attributes;
        }

        /// <summary>
        /// 增加空位
        /// </summary>
        /// <param name="strs_a"></param>
        /// <returns></returns>
        public List<string> AddVacancy(params string[] strs_a)
        {
            List<string> strs = new List<string>();

            for (int i = 0; i < strs_a.Length; i++)
            {
                strs.Add(string.Format(TabulatorStr, strs_a[i]));
            }
            return strs;
        }

        /// <summary>
        /// 拼接
        /// 方法
        /// 类
        /// 命名空间
        /// </summary>
        /// <param name="head"></param>
        /// <param name="body"></param>
        public List<string> SplicingBraces(string head, List<string> body)
        {
            List<string> strs = new List<string>();
            strs.Add(head);
            strs.Add("{");
            strs.AddRange(body);
            strs.Add("}");
            return strs;
        }

        /// <summary>
        /// 拼接
        /// 方法
        /// 类
        /// 命名空间
        /// </summary>
        /// <param name="head"></param>
        /// <param name="body"></param>
        /// <returns></returns>
        public List<string> SplicingBracesAddVacancy(string head, params List<string>[] body)
        {
            List<string> strs = new List<string>();
            strs.Add(head);
            strs.Add("{");
            for (int i = 0; i < body.Length; i++)
            {
                strs.AddRange(AddVacancy(body[i]));
            }
            strs.Add("}");
            return strs;
        }

        /// <summary>
        /// 拼接
        /// 方法
        /// 类
        /// 命名空间
        /// </summary>
        /// <param name="head"></param>
        /// <param name="body"></param>
        public List<string> SplicingBraces(string head, params string[] body)
        {
            List<string> strs = new List<string>();
            strs.Add(head);
            strs.Add("{");
            strs.AddRange(AddVacancy(body));
            strs.Add("}");
            return strs;
        }

        #endregion
    }
}
