﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Plus
{
    public static class StringPlusExpand
    {
        public static void DataByString<T>(this Dictionary<int, List<T>> dic, ref Dictionary<int, List<T>> dic2, string str)
        {
            dic2 = StringPlus.Instance.DictionaryByString<T>(str);
        }

        public static void DataByString<T>(this List<T> list, ref List<T> list2, string str)
        {
            list2 = StringPlus.Instance.ListByString<T>(str);
        }

        public static void DataByString<T>(this T t, ref T t2, string str)
        {
            t2 = StringPlus.Instance.TByString<T>(str);
        }
        public static Dictionary<int, List<T>> DataByString<T>(this Dictionary<int, List<T>> dic, string str)
        {
            return StringPlus.Instance.DictionaryByString<T>(str);
        }

        public static List<T> DataByString<T>(this List<T> list, string str)
        {
            return StringPlus.Instance.ListByString<T>(str);
        }

        public static T DataByString<T>(this T t, string str)
        {
            return StringPlus.Instance.TByString<T>(str);
        }
        public static T DataByString<T>(Type t, string str)
        {
            return StringPlus.Instance.TByString<T>(str);
        }

        public static T DataByString<T>(this string str)
        {
            return StringPlus.Instance.TByString<T>(str);
        }

        public static string DataToString<T>(this Dictionary<int, List<T>> dic)
        {
            return StringPlus.Instance.StringByDictionary<T>(dic);
        }

        public static string DataToString<T>(this List<T> list)
        {
            return StringPlus.Instance.StringByList<T>(list);
        }

        public static string DataToString<T>(this T t)
        {
            return StringPlus.Instance.StringByT<T>(t);
        }

        public static void Demo()
        {
            Dictionary<int, List<float>> dic = new Dictionary<int, List<float>>();
            List<float> list = new List<float>();
            float f = 1.0f;

            string strDic = dic.DataToString();
            string strList = list.DataToString();
            string strFloat = f.DataToString();

            dic.DataByString(ref dic, strDic);
            dic = dic.DataByString(strDic);
            list.DataByString(ref list, strList);
            f.DataByString(ref f, strFloat);

        }

        public static bool IsNull(this string str)
        {
            return str == null || str == "";
        }
    }

}
