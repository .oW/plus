﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Plus
{
    public class Singleton<T>
        where T : Singleton<T>, new()
    {
        private static T instance;

        public static T Instance
        {
            get
            {
                if(instance == null)
                {
                    instance = new T();
                    instance.Init();
                }
                return instance;
            }
        }

        private void Init()
        {
            OnInit();
        }

        protected virtual void OnInit()
        {

        }
    }
}
