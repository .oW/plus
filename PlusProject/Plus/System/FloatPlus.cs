﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Plus
{
    public static class FloatPlus
    {
        public static System.Random Ram {
            get
            {
                if(random == null)
                {
                    random = new System.Random();
                }
                return random;
            }
        }

        private static System.Random random;

        /// <summary>
        /// 转换为正数
        /// </summary>
        /// <param name="num"></param>
        /// <returns></returns>
        public static float ToU(float num)
        {
            return num > 0 ? num : -num;
        }

        /// <summary>
        /// 转换为负数
        /// </summary>
        /// <param name="num"></param>
        /// <returns></returns>
        public static float ToD(float num)
        {
            return num < 0 ? num : -num;
        }

        public static float DifferenceU(float now, float max)
        {
            return ToU(max - now);
        }

        public static int ChangeRange(ref float now, float max, float f_c)
        {
            int flag = 0;
            if (DifferenceU(now, max) > ToU(f_c))
            {
                now += f_c;
                flag = 1;
            }
            return flag;
        }
        public static float GetRange(float now, float max, float time_c)
        {
            float c = (max - now)* time_c;
            return c;
        }
        public static void GetRangeByCamera(float _max, float fieldOfView, float s, out float max, out float now, out float c, float time_c)
        {
            max = _max / s;
            now = fieldOfView / s;
            c = GetRange(now, max, time_c);
        }

        /// <summary>
        /// 保留精度
        /// 没有四舍五入
        /// </summary>
        /// <returns></returns>
        public static float KeepDecimal(float f,int coumt)
        {
            if(coumt >= 8)
            {
                return f;
            }
            byte[] bytes = BytePlus.GetByte(f, coumt);
            float f_ = BytePlus.GetFloat(bytes, coumt);
            return f_;
        }

        /// <summary>
        /// 保留精度
        /// 有四舍五入
        /// </summary>
        /// <returns></returns>
        public static float KeepDecimalExtend(float f, int count)
        {
            if (count >= 8)
            {
                return f;
            }
            int num_c = IntPlus.Power(10, count + 1);
            float n = f * num_c;

            float num = n % 10;
            if(num >= 5)
            {
                n += 10;
            }
            n /= num_c;
            byte[] bytes = BytePlus.GetByte(n, count);
            float f_ = BytePlus.GetFloat(bytes,count);
            return f_;
        }

        /// <summary>
        /// 
        /// flag == 1 上升
        /// flag == 0 下降
        /// </summary>
        /// <param name="min"></param>
        /// <param name="max"></param>
        /// <param name="c"></param>
        /// <param name="now"></param>
        /// <param name="flag"></param>
        public static void ClosedLoopInterval(float min,float max, float c,ref float now,ref int flag)
        {
            int now_flag = 0;
            if (flag == 1)
            {
                now_flag = ChangeRange(ref now,max,c);
            }
            else
            {
                now_flag = ChangeRange(ref now, min, c);
            }

            if(now_flag == 0)
            {
                c = -c;
                flag = flag == 1 ? 0 : 1;
            }
        }
        /// <summary>
        /// 
        /// flag == 1 上升
        /// flag == 0 下降
        /// </summary>
        /// <param name="min"></param>
        /// <param name="max"></param>
        /// <param name="c"></param>
        /// <param name="now"></param>
        /// <param name="flag"></param>
        public static void ClosedLoopInterval(float min, float max, float c, ref float now, ref int flag,ref float rang_flag)
        {

            if(flag == 1)
            {
                if(Ram.Next(0,100)*0.01f <= rang_flag)
                {
                    c = -c;
                    flag = 0;
                    rang_flag = 0;
                }
            }
            else
            {
                rang_flag = 0;
                c = -c;
            }
            int now_flag = 0;
            if (flag == 1)
            {
                now_flag = ChangeRange(ref now, max, c);
            }
            else
            {
                now_flag = ChangeRange(ref now, min, c);
            }

            if (now_flag == 0)
            {
                c = -c;
                flag = flag == 1 ? 0 : 1;
            }

        }
        #region 时间格式转换

        /// <summary>
        /// 时间格式转换
        /// </summary>
        /// <returns></returns>
        public static string TimeFormatConversion(this float time)
        {
            time = time / 1000;
            int hour = (int)(time / 60 / 60 % 24);
            int minute = (int)(time / 60 % 60);
            int second = (int)(time % 60);
            string hourStr = GetTimeFormatConversionStr(hour);
            string minuteStr = GetTimeFormatConversionStr(minute);
            string secondStr = GetTimeFormatConversionStr(second);
            string timeStr = string.Format("{0} : {1} : {2}", hourStr, minuteStr, secondStr);
            return timeStr;
        }
        private static string GetTimeFormatConversionStr(int num)
        {
            return num < 10 ? string.Format("0{0}", num) : num.ToString();
        } 
        #endregion
    }

}