﻿using Plus;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Data
{
    /// <summary>
    /// 数据类的父类
    /// </summary>
    public class CSVDataBase
    {
        protected StringPlus sP => StringPlus.Instance;

        #region 属性

        private int _uid;
        private int _id;
        private string _eName;
        private string _cName;
        private string _desc;

        public virtual int GetImportantId => Id;
        public virtual string GetImportantName => E_name;

        public int Uid { get => _uid; set => _uid = value; }
        public int Id { get => _id; set => _id = value; }
        public string E_name { get => _eName; set => _eName = value; }
        public string C_name { get => _cName; set => _cName = value; }
        public string Desc { get => _desc; set => _desc = value; }

        #endregion

        #region 构造

        public CSVDataBase()
        {

        }

        #endregion

        #region 基础方法

        public void Set(List<string> list)
        {
            int i = 0;
            Uid = sP.TByString<int>(list[i]);
            Id = sP.TByString<int>(list[++i]);
            E_name = list[++i];
            C_name = list[++i];
            Desc = list[++i];
            OnSet(list);
        }
        public List<string> Get()
        {
            List<string> list = new List<string>();

            list.Add(sP.StringByT(Uid));
            list.Add(sP.StringByT(Id));
            list.Add(E_name);
            list.Add(C_name);
            list.Add(Desc);
            return OnGet(list);
        }
        #endregion

        #region 子类需要重写

        protected virtual void OnSet(List<string> list)
        {
        }

        protected virtual List<string> OnGet(List<string> list)
        {
            return list;
        }

        #endregion
    }
}
