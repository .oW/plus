﻿using Data;
using Plus.GFile;
using Plus.GIO;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Plus.Data
{
    public static class DataBasePlus
    {
        /// <summary>
        /// 前置读取行数
        /// </summary>
        public const int PreReadCount = 4;

        #region 克隆数据

        /// <summary>
        /// 克隆数据
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="_this"></param>
        /// <returns></returns>
        public static T Clone<T>(T _this)
            where T : CSVDataBase, new()
        {
            T t = new T();
            t.Set(_this.Get());
            return t;
        }

        #endregion

        #region 获取数据

        #region Dictionary<int, List<T>>
        /// <summary>
        /// 获取全部数据
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="path"></param>
        /// <param name="name"></param>
        /// <returns></returns>
        public static Dictionary<int, List<T>> GetDictionaryListDataBase<T>(string path, string name, DirectoryType type)
            where T : CSVDataBase, new()
        {
            Dictionary<int, List<T>> butD = new Dictionary<int, List<T>>();

            List<T> butDs = GetList<T>(path, name, type);
            List<T> bdss = new List<T>();
            int ind = -1;
            for (int i = 0; i < butDs.Count; i++)
            {
                T pl = butDs[i];
                if (ind != pl.GetImportantId)
                {
                    if (ind != -1)
                    {
                        Store<T>(ref bdss, ind, ref butD);
                    }
                    ind = pl.GetImportantId;
                }
                bdss.Add(pl);
            }
            Store<T>(ref bdss, ind, ref butD);

            return butD;
        }
        /// <summary>
        /// 获取全部数据
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="path"></param>
        /// <param name="name"></param>
        /// <returns></returns>
        public static Dictionary<int, List<T>> GetDicListInt<T>(string path, string name)
            where T : CSVDataBase, new()
        {
            DirectoryType type = path.GetDirectoryType();
            Dictionary<int, List<T>> butD = new Dictionary<int, List<T>>();

            List<T> butDs = GetList<T>(path, name, type);
            List<T> bdss = new List<T>();
            int ind = -1;
            for (int i = 0; i < butDs.Count; i++)
            {
                T pl = butDs[i];
                if (ind != pl.GetImportantId)
                {
                    if (ind != -1)
                    {
                        Store<T>(ref bdss, ind, ref butD);
                    }
                    ind = pl.GetImportantId;
                }
                bdss.Add(pl);
            }
            Store<T>(ref bdss, ind, ref butD);

            return butD;
        }
        #endregion

        #region Dictionary<int, T>
        public static Dictionary<int, T> GetDicInt<T>(string path, string name, DirectoryType type)
            where T : CSVDataBase, new()
        {
            Dictionary<int, T> butD = new Dictionary<int, T>();

            List<T> butDs = GetList<T>(path, name, type);
            for (int i = 0; i < butDs.Count; i++)
            {
                T pl = butDs[i];
                if (butD.ContainsKey(pl.GetImportantId))
                {
                    continue;
                }
                butD.Add(pl.GetImportantId, pl);
            }

            return butD;
        }
        public static Dictionary<int, T> GetDicInt<T>(string path, string name)
            where T : CSVDataBase, new()
        {
            DirectoryType type = path.GetDirectoryType();
            Dictionary<int, T> butD = new Dictionary<int, T>();

            List<T> butDs = GetList<T>(path, name, type);
            for (int i = 0; i < butDs.Count; i++)
            {
                T pl = butDs[i];
                if (butD.ContainsKey(pl.GetImportantId))
                {
                    continue;
                }
                butD.Add(pl.GetImportantId, pl);
            }

            return butD;
        }
        #endregion

        #region Dictionary<string, T>

        public static Dictionary<string, T> GetDicStr<T>(string path, string name, DirectoryType type)
            where T : CSVDataBase, new()
        {
            Dictionary<string, T> butD = new Dictionary<string, T>();

            List<T> butDs = GetList<T>(path, name, type);
            for (int i = 0; i < butDs.Count; i++)
            {
                T pl = butDs[i];
                if (butD.ContainsKey(pl.GetImportantName))
                {
                    continue;
                }
                butD.Add(pl.GetImportantName, pl);
            }

            return butD;
        }
        #endregion

        #region Dictionary<string, List<T>>
        public static Dictionary<string, List<T>> GetDicListStr<T>(string path, string name, DirectoryType type)
            where T : CSVDataBase, new()
        {
            Dictionary<string, List<T>> butD = new Dictionary<string, List<T>>();

            List<T> butDs = GetList<T>(path, name, type);
            for (int i = 0; i < butDs.Count; i++)
            {
                T pl = butDs[i];
                if (butD.ContainsKey(pl.GetImportantName))
                {
                    butD[pl.GetImportantName].Add(pl);
                    continue;
                }
                butD.Add(pl.GetImportantName, new List<T>());
                butD[pl.GetImportantName].Add(pl);
            }

            return butD;
        }
        public static Dictionary<string, List<T>> GetDicListStr<T>(string path, string name)
            where T : CSVDataBase, new()
        {
            DirectoryType type = path.GetDirectoryType();
            Dictionary<string, List<T>> butD = new Dictionary<string, List<T>>();

            List<T> butDs = GetList<T>(path, name, type);
            for (int i = 0; i < butDs.Count; i++)
            {
                T pl = butDs[i];
                if (butD.ContainsKey(pl.GetImportantName))
                {
                    butD[pl.GetImportantName].Add(pl);
                    continue;
                }
                butD.Add(pl.GetImportantName, new List<T>());
                butD[pl.GetImportantName].Add(pl);
            }

            return butD;
        }
        public static Dictionary<string, T> GetDicStr<T>(string path, string name)
            where T : CSVDataBase, new()
        {
            DirectoryType type = path.GetDirectoryType();
            Dictionary<string, T> butD = new Dictionary<string, T>();

            List<T> butDs = GetList<T>(path, name, type);
            for (int i = 0; i < butDs.Count; i++)
            {
                T pl = butDs[i];
                if (butD.ContainsKey(pl.GetImportantName))
                {
                    continue;
                }
                butD.Add(pl.GetImportantName, pl);
            }

            return butD;
        }
        #endregion

        #region List<T>

        public static List<T> GetList<T>(string path, string name, DirectoryType type)
            where T : CSVDataBase, new()
        {
            List<T> list = new List<T>();
            List<List<string>> vs = new List<List<string>>();
            type = path.GetDirectoryType();
            if (name == null)
            {
                name = GetClassName<T>();
            }
            GetCSV(path, name, ref vs, type);
            for (int i = PreReadCount; i < vs.Count; i++)
            {
                T pl = new T();
                pl.Set(vs[i]);
                list.Add(pl);
            }
            for (int i = 0; i < list.Count; i++)
            {
                for (int j = i + 1; j < list.Count; j++)
                {
                    if (list[i].GetImportantId > list[j].GetImportantId)
                    {
                        T pl = list[i];
                        list[i] = list[j];
                        list[j] = pl;
                    }
                }
            }
            return list;
        }

        public static List<T> GetListByCSV<T>(string path, DirectoryType type = DirectoryType.Assets)
            where T : CSVDataBase, new()
        {
            string name = GetClassName<T>();
            return GetList<T>(path, name, type);
        }

        #endregion

        #region 类名和表名一致获取数据

        public static Dictionary<int, List<T>> GetDictionaryListDataBase<T>(string path, DirectoryType type)
            where T : CSVDataBase, new()
        {
            string name = GetClassName<T>();

            return GetDictionaryListDataBase<T>(path, name, type);
        }
        public static Dictionary<int, T> GetDictionaryDataBase<T>(string path, DirectoryType type)
            where T : CSVDataBase, new()
        {
            string name = GetClassName<T>();

            return GetDicInt<T>(path, name, type);
        }
        public static List<T> GetDataBaseAll<T>(string path, DirectoryType type)
            where T : CSVDataBase, new()
        {
            return GetList<T>(path, GetClassName<T>(), type);
        }

        #endregion

        /// <summary>
        /// 排序
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="list_flag"></param>
        /// <param name="ind"></param>
        /// <param name="dic"></param>
        private static void Store<T>(ref List<T> list_flag, int ind, ref Dictionary<int, List<T>> dic)
        {
            List<T> list = new List<T>();
            list.AddRange(list_flag);
            dic.Add(ind, list);
            list_flag.Clear();
        }

        /// <summary>
        /// 根据ID获取值
        /// </summary>
        /// <typeparam name="V"></typeparam>
        /// <param name="data"></param>
        /// <param name="iId"></param>
        /// <param name="id"></param>
        /// <returns></returns>
        public static V GetValueById<V>(Dictionary<int, List<V>> data, int iId, int id)
            where V : CSVDataBase
        {
            V v = default(V);

            v = GetValueById<V>(data[iId], id);

            return v;
        }
        public static V GetValueById<V>(Dictionary<int, V> data, int id)
           where V : CSVDataBase
        {
            V v = null;

            foreach (var item in data)
            {
                if (item.Value.Id == id)
                {
                    v = item.Value;
                    break;
                }
            }

            return v;
        }

        /// <summary>
        /// 获取下一个空的ID
        /// </summary>
        /// <returns></returns>
        public static int GetNullIndexByKey<D>(this Dictionary<int, D> ts)
            where D : CSVDataBase
        {
            Dictionary<int, D> ds = SortDictionaryT(ts);
            int now = 1;

            foreach (var item in ds)
            {
                if (item.Value.Id == now)
                {
                    ++now;
                }
                else
                {
                    break;
                }
            }
            if (ds.ContainsKey(now))
            {
                ++now;
            }

            return now;
        }

        public static int GetNullIndexByKey<D>(this Dictionary<int, List<D>> ts)
           where D : CSVDataBase
        {
            Dictionary<int, D> ds = SortDictionaryT(ts);
            int now = 1;

            foreach (var item in ds)
            {
                if (item.Value.Id == now)
                {
                    ++now;
                }
                else
                {
                    break;
                }
            }

            return now;
        }

        public static Dictionary<int, D> SortDictionaryT<D>(Dictionary<int, D> ts)
           where D : CSVDataBase
        {
            Dictionary<int, D> data = new Dictionary<int, D>();
            List<int> keys = ts.Keys.ToList();
            for (int i = 0; i < keys.Count; i++)
            {
                for (int j = i + 1; j < keys.Count; j++)
                {
                    if (keys[i] >= keys[j])
                    {
                        int num = keys[i];
                        keys[i] = keys[j];
                        keys[j] = num;
                    }
                }
            }
            for (int i = 0; i < keys.Count; i++)
            {
                data.Add(keys[i], ts[keys[i]]);
            }
            return data;
        }
        public static Dictionary<int, D> SortDictionaryT<D>(Dictionary<int, List<D>> ts)
           where D : CSVDataBase
        {
            Dictionary<int, D> data = new Dictionary<int, D>();
            Dictionary<int, D> data2 = new Dictionary<int, D>();
            List<int> keys = new List<int>();
            foreach (var item in ts)
            {
                for (int i = 0; i < item.Value.Count; i++)
                {
                    keys.Add(item.Value[i].Id);
                    data2.Add(item.Value[i].Id, item.Value[i]);
                }
            }
            for (int i = 0; i < keys.Count; i++)
            {
                for (int j = i + 1; j < keys.Count; j++)
                {
                    if (keys[i] >= keys[j])
                    {
                        int num = keys[i];
                        keys[i] = keys[j];
                        keys[j] = num;
                    }
                }
            }
            for (int i = 0; i < keys.Count; i++)
            {
                data.Add(keys[i], data2[keys[i]]);
            }
            return data;
        }

        public static V GetValueById<V>(List<V> data, int id)
           where V : CSVDataBase
        {
            V v = null;

            for (int i = 0; i < data.Count; i++)
            {
                if (data[i].Id == id)
                {
                    v = data[i];
                    break;
                }
            }

            return v;
        }


        #endregion

        #region 储存数据

        /// <summary>
        /// 保存数据
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="data"></param>
        /// <param name="name"></param>
        public static void SaveDicListData<T>(Dictionary<int, List<T>> data, string path, string name, DirectoryType type)
            where T : CSVDataBase, new()
        {
            List<List<string>> vs = new List<List<string>>();
            GetCSV(path, name, ref vs, type);
            SetBackUpCSV(path, name, vs, type);
            for (int j = vs.Count - 1; j >= PreReadCount; j--)
            {
                vs.RemoveAt(j);
            }
            foreach (var item in data)
            {
                if (item.Key > 0)
                {
                    for (int j = 0; j < item.Value.Count; j++)
                    {
                        vs.Add(item.Value[j].Get());
                    }
                }
            }
            SetCSV(path, name, vs, type);
        }
        public static void SaveData<T>(Dictionary<int, T> data, string path, string name, DirectoryType type)
           where T : CSVDataBase, new()
        {
            List<List<string>> vs = new List<List<string>>();
            GetCSV(path, name, ref vs, type);
            SetBackUpCSV(path, name, vs, type);
            for (int j = vs.Count - 1; j >= PreReadCount; j--)
            {
                vs.RemoveAt(j);
            }
            foreach (var item in data)
            {
                if (item.Key > 0)
                {
                    vs.Add(item.Value.Get());
                }
            }
            SetCSV(path, name, vs, type);
        }
        public static void SaveListDaa<T>(List<T> data, string path, string name, DirectoryType type)
            where T : CSVDataBase, new()
        {
            List<List<string>> vs = new List<List<string>>();
            GetCSV(path, name, ref vs, type);
            SetBackUpCSV(path, name, vs, type);
            for (int j = vs.Count - 1; j >= PreReadCount; j--)
            {
                vs.RemoveAt(j);
            }
            for (int j = 0; j < data.Count; j++)
            {
                vs.Add(data[j].Get());
            }
            SetCSV(path, name, vs, type);
        }// <summary>
        /// 保存数据
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="data"></param>
        /// <param name="name"></param>
        public static void SaveListData<T>(List<T> data, string path, DirectoryType type)
            where T : CSVDataBase, new()
        {

            if (data == null)
            {
                return;
            }
            string name = GetClassName<T>();
            SaveListDaa(data, path, name, type);
        }
        public static void SaveData<T>(Dictionary<int, T> data, string path, DirectoryType type)
           where T : CSVDataBase, new()
        {
            if (data == null)
            {
                return;
            }
            string name = GetClassName<T>();
            SaveData(data, path, name, type);
        }
        public static void SaveDicListData<T>(Dictionary<int, List<T>> data, string path, DirectoryType type)
           where T : CSVDataBase, new()
        {
            if (data == null)
            {
                return;
            }
            string name = GetClassName<T>();
            SaveDicListData(data, path, name, type);
        }

        #endregion

        #region 辅助的方法

        private static void GetCSV(string path, string name, ref List<List<string>> data, DirectoryType type)
        {
            if (path == "" || path == null)
            {
                FileManage.GetCSV(name, ref data, type);
            }
            else
            {
                FileManage.GetCSV(string.Format("{0}/{1}",path,name), ref data, type);
            }
        }
        private static void GetCSV(string name, ref List<List<string>> data, DirectoryType type)
        {
            FileManage.GetCSV(name, ref data, type);
        }
        private static void GetBackUpCSV(string path, string name, ref List<List<string>> data, DirectoryType type)
        {
            if (path == "" || path == null)
            {
                FileManage.GetCSV($"BackUp/{name}", ref data, type);
            }
            else
            {
                FileManage.GetCSV($"{path}/BackUp/{name}", ref data, type);
            }
        }
        private static void SetCSV(string path, string name, List<List<string>> data, DirectoryType type)
        {
            if (path == "" || path == null)
            {
                FileManage.SetCSV(name, data, type);
            }
            else
            {
                DirectoryPlus.AddDirectory(path);
                FileManage.SetCSV($"{path}/{name}", data, type);
            }
        }
        private static void SetBackUpCSV(string path, string name, List<List<string>> data, DirectoryType type)
        {
            if (path == "" || path == null)
            {
                DirectoryPlus.AddDirectory("BackUp");
                FileManage.SetCSV($"BackUp/{name}BackUp", data, type);
            }
            else
            {
                DirectoryPlus.AddDirectory($"{path}/BackUp");
                FileManage.SetCSV($"{path}/BackUp/{name}BackUp", data, type);
            }
        }

        public static string GetClassName<V>()
            where V : CSVDataBase, new()
        {
            string name = typeof(V).Name;

            int index = name.IndexOf("CSVData");
            if (index != -1)
            {
                name = name.Substring(0, name.IndexOf("CSVData"));
            }
            return name;

        }
        #endregion

        #region 扩展

        #region 编辑环境下的获取数据



        public static Dictionary<string, V> GetDicStr<V>(this V v, string path = "")
               where V : CSVDataBase, new()
        {
            Dictionary<string, V> dic = Dic<string, V>.dic;

            if (dic.Count == 0)
            {
                dic = GetDicStr<V>(path, GetClassName<V>());
            }

            return dic;
        }
        public static Dictionary<int, V> GetDicInt<V>(this V v, string path = "")
            where V : CSVDataBase, new()
        {
            Dictionary<int, V> dic = Dic<int, V>.dic;

            if (dic.Count == 0)
            {
                dic = GetDicInt<V>(path, GetClassName<V>());
            }

            return dic;
        }
        public static Dictionary<int, List<V>> GetDicListInt<V>(this V v, string path = "")
            where V : CSVDataBase, new()
        {
            Dictionary<int, List<V>> dic = Dic<int, List<V>>.dic;

            if (dic.Count == 0)
            {
                dic = GetDicListInt<V>(path, GetClassName<V>());
            }

            return dic;
        }
        public static Dictionary<string, List<V>> GetDicListStr<V>(this V v, string path = "")
                    where V : CSVDataBase, new()
        {
            Dictionary<string, List<V>> dic = Dic<string, List<V>>.dic;

            if (dic.Count == 0)
            {
                dic = GetDicListStr<V>(path, GetClassName<V>());
            }

            return dic;
        }
        public static List<V> GetLisInt<V>(this V v, string path, DirectoryType type)
                    where V : CSVDataBase, new()
        {
            List<V> dic = Lis<V>.lis;

            if (dic.Count == 0)
            {
                dic = GetList<V>(path, GetClassName<V>(), type);
            }

            return dic;
        }
        public static Dictionary<string, V> GetDicStr<V>(string path = "")
               where V : CSVDataBase, new()
        {
            Dictionary<string, V> dic = Dic<string, V>.dic;

            if (dic.Count == 0)
            {
                dic = GetDicStr<V>(path, GetClassName<V>());
            }

            return dic;
        }
        public static Dictionary<int, V> GetDicInt<V>(string path = "")
            where V : CSVDataBase, new()
        {
            Dictionary<int, V> dic = Dic<int, V>.dic;

            if (dic.Count == 0)
            {
                string name = GetClassName<V>();
                if(name.IndexOf("CSVData") != -1)
                {
                    name = name.Substring(0, name.IndexOf("CSVData"));
                }
                dic = GetDicInt<V>(path, GetClassName<V>());
            }

            return dic;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <typeparam name="V"></typeparam>
        /// <param name="path"></param>
        /// <returns></returns>
        public static Dictionary<int, V> GetDicData<V>(string path = "")
            where V : CSVDataBase, new()
        {
            Dictionary<int, V> dic = Dic<int, V>.dic;

            if (dic.Count == 0)
            {
                string name = GetClassName<V>();
                dic = GetDicInt<V>(path, name);
            }

            return dic;
        }
        public static Dictionary<int, List<V>> GetDicListInt<V>(string path = "")
            where V : CSVDataBase, new()
        {
            Dictionary<int, List<V>> dic = Dic<int, List<V>>.dic;

            if (dic.Count == 0)
            {
                dic = GetDicListInt<V>(path, GetClassName<V>());
            }

            return dic;
        }
        public static Dictionary<string, List<V>> GetDicListStr<V>(string path = "")
                    where V : CSVDataBase, new()
        {
            Dictionary<string, List<V>> dic = Dic<string, List<V>>.dic;
            if (dic.Count == 0)
            {
                dic = GetDicListStr<V>(path, GetClassName<V>());
            }

            return dic;
        }
        public static List<V> GetLisInt<V>(string path , DirectoryType type)
                    where V : CSVDataBase, new()
        {
            List<V> dic = Lis<V>.lis;

            if (dic.Count == 0)
            {
                dic = GetList<V>(path, GetClassName<V>(), type);
            }

            return dic;
        }

        public class Dic<K, V>
        {
            public static Dictionary<K, V> dic = new Dictionary<K, V>();
        }

        public class Lis<V>
        {
            public static List<V> lis = new List<V>();
        }


        #endregion

        public static DirectoryType GetDirectoryType(this string path)
        {
            DirectoryType type = DirectoryType.All;
            if (path == "" || path == null)
            {
                type = DirectoryType.Resources;
            }
            else
            {
                type = DirectoryType.Assets;
            }
            return type;
        }

        #endregion


        #region StringPlusData

        public static List<T> GetList<T>(this List<StringPlusData> datas)
            where T : new()
        {
            List<T> ts = new List<T>();

            return ts;
        }

        public static Dictionary<T, Y> GetDic<T, Y>(this List<StringPlusData> datas)
        {
            Dictionary<T, Y> ts = new Dictionary<T, Y>();

            return ts;
        }

        #endregion
    }

}
