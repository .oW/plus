﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Plus.Data
{
    public class StringPlusData
    {
        public List<string> names = new List<string>();
        public List<string> types = new List<string>();
        public List<string> values = new List<string>();

        public void SetData(string name, string type, string value)
        {
            int index = names.IndexOf(name);
            if (index == -1)
            {
                names.Add(name);
                types.Add(type);
                values.Add(value);
            }
            else
            {
                names[index] = name;
                types[index] = type;
                values[index] = value;
            }
        }

        public string GetValue(string name)
        {
            int index = names.IndexOf(name);
            if(index == -1)
            {
                return "";
            }
            else
            {
                return values[index];
            }
        }
        public string GetType(string name)
        {
            int index = names.IndexOf(name);
            if (index == -1)
            {
                return "";
            }
            else
            {
                return types[index];
            }
        }
    }
}
