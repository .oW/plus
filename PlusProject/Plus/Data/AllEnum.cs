﻿public enum PathType
{
    Line,
    Bezier2,
    Bezier3,
}

public enum RepeatType
{
    Once,
    Loop,
    PingPang,
}

//存放的路径
public enum DirectoryType
{
    Assets = 0,
    Resources = 1,
    All = 2,
}