﻿using Plus.GIO.Data;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Plus.GIO
{
    //存放的路径
    //public static class DirectoryType
    //{
    //    public const string
    //    ;
    //}
    public static class MainPath
    {
        public const string
        Assets = "Assets"
        ;
    }
    public static class FileType
    {
        public const string
        png = "png",
        csv = "csv",
        mp3 = "mp3",
        ogg = "ogg",
        txt = "txt",
        prefab = "prefab",
        xlsx = "xlsx",
        xlk = "xlk"
        ;
    }
    public static class DirectoryPlus
    {
        public static string GetString(this DirectoryType type)
        {
            string str = "";
            switch (type)
            {
                case DirectoryType.Assets:
                    str = "Assets";
                    break;
                case DirectoryType.Resources:
                    str = "Resources";
                    break;
                case DirectoryType.All:
                    str = "All";
                    break;
                default:
                    break;
            }
            return str;
        }
        public static void AddDirectory(string path, params string[] strs)
        {
            if (!Directory.Exists(path))
            {
                Directory.CreateDirectory(path);
            }
            for (int i = 0; i < strs.Length; i++)
            {
                path += ($"/{strs[i]}");
                if (!Directory.Exists(path))
                {
                    Directory.CreateDirectory(path);
                }
            }
        }
        /// <summary>
        /// 获取规定的文件
        /// </summary>
        /// <param name="dType">文件夹名</param>
        /// <param name="fType">文件类型</param>
        public static List<FileData> GetTypes(DirectoryType dType, string fType, string path)
        {
            List<FileData> fileds = new List<FileData>();
            if (dType == DirectoryType.All)
            {
                //获取指定路径下面的所有资源文件  
                fileds.AddRange(GetTypes(path, fType));
            }
            else
            {
                List<string> paths = GetDirectorys(dType, path);
                for (int i = 0; i < paths.Count; i++)
                {
                    fileds.AddRange(GetTypes(paths[i], fType));
                }
                DirectoryInfo direction = new DirectoryInfo(path);
                DirectoryInfo[] dires = direction.GetDirectories("*", SearchOption.AllDirectories);

            }
            return fileds;
        }
        /// <summary>
        /// 获取规定的文件
        /// </summary>
        /// <param name="path"></param>
        /// <param name="fType"></param>
        /// <returns></returns>
        public static List<FileData> GetTypes(string path, string fType)
        {
            List<FileData> fileds = new List<FileData>();
            if (Directory.Exists(path))
            {
                DirectoryInfo direction = new DirectoryInfo(path);
                FileInfo[] files = direction.GetFiles("*", SearchOption.AllDirectories);

                Console.WriteLine(string.Format("文件数量____{0}", files.Length));

                for (int i = 0; i < files.Length; i++)
                {
                    if (!files[i].Name.EndsWith($".{fType}"))
                    {
                        continue;
                    }
                    string name_f = files[i].Name;
                    int index = name_f.LastIndexOf(".");
                    string name = name_f.Substring(0, index);
                    fileds.Add(new FileData(name, fType, files[i].FullName));
                }
            }
            return fileds;
        }

        public static List<string> GetDirectorys(DirectoryType dType, string path)
        {
            List<string> paths = new List<string>();

            DirectoryInfo direction = new DirectoryInfo(path);
            DirectoryInfo[] dires = direction.GetDirectories("*", SearchOption.AllDirectories);
            for (int i = 0; i < dires.Length; i++)
            {
                Console.WriteLine(dires[i].FullName);
                if (dires[i].Name != dType.GetString())
                {
                    continue;
                }
                paths.Add(dires[i].FullName);
            }
            return paths;
        }


        /// <summary>
        /// 创建文件夹
        /// </summary>
        /// <param name="path"></param>
        public static void CreateDirectory(string path)
        {
            if (!Directory.Exists(path))
            {
                Directory.CreateDirectory(path);
            }
        }

        /// <summary>
        /// 打开文件夹
        /// </summary>
        /// <param name="path"></param>
        public static void OpenFolder(string path)
        {
            if (!Directory.Exists(path))
            {
                Directory.CreateDirectory(path);
            }
            path = path.Replace("/", "\\");
            System.Diagnostics.Process.Start("explorer.exe", path);

        }
    }
}
