﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Plus.GIO.Data
{
    public class FileData
    {
        public string name;
        public string type;
        public string path;

        public FileData()
        {
        }

        public FileData(string name, string type, string path)
        {
            this.name = name;
            this.type = type;
            this.path = path;
        }
    }
}
