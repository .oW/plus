﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Plus.GRange
{
    public class FloatRangData : RangeData<float>
    {
        public override int ChangeRange()
        {
            return FloatPlus.ChangeRange(ref now, Max, c);
        }
        protected override float GetRange(float now, float max, float time)
        {
            return FloatPlus.GetRange(now, max, time);
        }
    }
}
