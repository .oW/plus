﻿using EditorWindowUIExpand;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEditor;
using UnityEngine;

namespace Game.Plus.Editor
{
    public static class GUILayoutPlus
    {
        /// <summary>
        /// 间隔
        /// </summary>
        public const int interval = 10;

        #region UIStyle

        #region 原始

        #region 分段

        public static void DrawGuiInBoxDivider()
        {

            GUILayout.Space(8f);

            if (Event.current.type == EventType.Repaint)
            {

                int extra = 0;
#if UNITY_4_3
				extra = 10;
#endif

                Texture2D tex = EditorGUIUtility.whiteTexture;
                Rect rect = GUILayoutUtility.GetLastRect();
                UnityEngine.GUI.color = new Color(0.5f, 0.5f, 0.5f, 0.25f);
                UnityEngine.GUI.DrawTexture(new Rect(5f + extra, rect.yMin + 5f, Screen.width - 11, 1f), tex);
                UnityEngine.GUI.color = Color.white;
            }
        }

        public static void DrawGuiDivider()
        {

            GUILayout.Space(12f);

            if (Event.current.type == EventType.Repaint)
            {
                Texture2D tex = EditorGUIUtility.whiteTexture;
                Rect rect = GUILayoutUtility.GetLastRect();
                UnityEngine.GUI.color = new Color(0f, 0f, 0f, 0.25f);
                UnityEngine.GUI.DrawTexture(new Rect(0f, rect.yMin + 6f, Screen.width, 4f), tex);
                UnityEngine.GUI.DrawTexture(new Rect(0f, rect.yMin + 6f, Screen.width, 1f), tex);
                UnityEngine.GUI.DrawTexture(new Rect(0f, rect.yMin + 9f, Screen.width, 1f), tex);
                UnityEngine.GUI.color = Color.white;
            }
        }

        #endregion

        #region Text

        public static GUIStyle TitleStyle(int Height = 16)
        {
            GUIStyle title = new GUIStyle(EditorStyles.largeLabel);

            title.fixedHeight = Height;

            title.SetFontStyle(0.7f, Height);

            title.clipping = TextClipping.Overflow;

            return title;
        }

        #endregion

        #region Button

        public static GUIStyle ThinButtonStyle(float Height = 24f, FontStyle style = FontStyle.Normal)
        {
            GUIStyle thinButton = new GUIStyle(EditorStyles.toolbarButton);
            thinButton.fontStyle = style;
            thinButton.fixedHeight = Height;
            thinButton.SetFontStyle();


            return thinButton;
        }

        public static GUIStyle ThinButtonRedStyle()
        {
            GUIStyle thinButtonRed = new GUIStyle(EditorStyles.toolbarButton);
            thinButtonRed.fontStyle = FontStyle.Bold;
            thinButtonRed.fixedHeight = 24f;
            thinButtonRed.normal.textColor = Color.red;
            return thinButtonRed;
        }

        public static GUIStyle ThinButtonPressedStyle()
        {
            GUIStyle thinButtonPressed = new GUIStyle(EditorStyles.toolbarButton);
            thinButtonPressed.fontStyle = FontStyle.Bold;
            thinButtonPressed.fixedHeight = 24f;
            return thinButtonPressed;
        }


        public static GUIStyle DropDownButtonStyle()
        {
            GUIStyle dropDownButton = new GUIStyle(EditorStyles.toolbarDropDown);
            dropDownButton.fontStyle = FontStyle.Bold;
            dropDownButton.fixedHeight = 20f;
            return dropDownButton;
        }

        public static GUIStyle EnumStyleButton()
        {
            GUIStyle enumStyleButton = new GUIStyle(EditorStyles.toolbarDropDown);
            enumStyleButton.onActive.background = ThinButtonStyle().onActive.background;
            enumStyleButton.fixedHeight = 24f;
            return enumStyleButton;
        }

        public static GUIStyle FoldOutButtonStyle()
        {
            GUIStyle foldOutButton = new GUIStyle(EditorStyles.foldout);
            foldOutButton.fontStyle = FontStyle.Bold;
            return foldOutButton;
        }

        #endregion

        #region Popup
        public static GUIStyle ThinPopupStyle(float Height = 24f, FontStyle style = FontStyle.Normal)
        {
            GUIStyle thinPopup = new GUIStyle(EditorStyles.toolbarPopup);
            thinPopup.fontStyle = style;
            thinPopup.fixedHeight = Height;
            thinPopup.SetFontStyle();
            return thinPopup;
        }
        #endregion

        #region Horizontal
        public static GUIStyle ThinHorizontalStyle(float Height = 24f, FontStyle style = FontStyle.Normal)
        {
            GUIStyle thinPopup = new GUIStyle();
            thinPopup.fontStyle = style;
            thinPopup.fixedHeight = Height;
            thinPopup.SetFontStyle();
            return thinPopup;
        }
        #endregion

        #region Space

        public static void GSpace(this float f)
        {
            GUILayout.Space(f);
        }
        #endregion

        #endregion

        #region 扩展 后续封装

        #region Int no

        /// <summary>
        /// Int
        /// </summary>
        /// <param name="name"></param>
        /// <param name="i"></param>
        /// <returns></returns>
        public static int EUIInt(this string name, int i)
        {
            GUILayout.BeginHorizontal();
            name.EditorTextStyle();
            i = EditorGUILayout.IntField(i);
            GUILayout.EndHorizontal();

            return i;
        }

        #endregion

        #region bool no

        /// <summary>
        /// bool
        /// </summary>
        /// <param name="name"></param>
        /// <param name="i"></param>
        /// <returns></returns>
        public static bool EUIBool(this string name, bool b)
        {
            GUILayout.BeginHorizontal();
            name.EditorTextStyle();
            b = EditorGUILayout.Toggle(b);
            GUILayout.EndHorizontal();

            return b;
        }

        #endregion

        #region Text
        /// <summary>
        /// Text 显示
        /// </summary>
        /// <param name="name"></param>
        /// <param name="style"></param>
        /// <param name="pixels"></param>
        public static void EUIText(this string name, GUIStyle style, float pixels = 9f)
        {

            EditorGUILayout.LabelField($"{name} :", style);
            GUILayout.Space(pixels);
        }
        /// <summary>
        /// Text 显示
        /// </summary>
        /// <param name="name"></param>
        /// <param name="style"></param>
        /// <param name="pixels"></param>
        public static void EUITextN(this string name, GUIStyle style, float pixels = 9f)
        {

            EditorGUILayout.LabelField(name, style);
            GUILayout.Space(pixels);
        }

        #endregion

        #region Popus

        /// <summary>
        /// Popus
        /// </summary>
        /// <param name="names"></param>
        /// <param name="style"></param>
        /// <param name="index"></param>
        /// <param name="actionChanger"></param>
        public static void EUIPopus(this string[] names, GUIStyle style, ref int index, Action actionChanger)
        {
            int indexNow = EditorGUILayout.Popup("", index, names, style);

            if (indexNow != index)
            {
                if (actionChanger != null)
                {
                    actionChanger();
                }
                index = indexNow;
            }
        }
        /// <summary>
        /// Popus
        /// </summary>
        /// <param name="names"></param>
        /// <param name="style"></param>
        /// <param name="index"></param>
        /// <param name="actionChanger"></param>
        public static void EUIPopus(this string[] names, GUIStyle style, ref int index, Action<int> actionChanger)
        {
            int indexNow = EditorGUILayout.Popup("", index, names, style);

            if (indexNow != index)
            {
                if (actionChanger != null)
                {
                    actionChanger(indexNow);
                }
                index = indexNow;
            }
        }

        #endregion

        #region Button

        /// <summary>
        /// Button
        /// 显示
        /// </summary>
        /// <param name="name"></param>
        /// <param name="style"></param>
        /// <param name="action"></param>
        public static void EUIButton(this string name, GUIStyle style, Action action)
        {
            if (GUILayout.Button(name, style))
            {
                if (action != null)
                {
                    action();
                }
            }
        } 
        #endregion

        #region Texture2D no

        /// <summary>
        /// Texture2D
        /// 图片
        /// </summary>
        /// <param name="texture"></param>
        /// <returns></returns>
        public static Texture2D EUITexture2D(this Texture2D texture)
        {
            texture = (Texture2D)EditorGUILayout.ObjectField(texture, typeof(Texture2D), true);
            GUILayout.Label(texture, GUILayout.Width(512), GUILayout.Height(256));
            return texture;
        }

        /// <summary>
        /// Texture2D
        /// 图片
        /// </summary>
        /// <param name="texture"></param>
        /// <returns></returns>
        public static Texture2D EUITexture2D(this string name, Texture2D texture)
        {

            GUILayout.BeginHorizontal();
            name.EditorTextStyle();
            texture = (Texture2D)EditorGUILayout.ObjectField(texture, typeof(Texture2D), true);
            GUILayout.EndHorizontal();
            GUILayout.Space(10);
            GUILayout.Label(texture, GUILayout.Width(512), GUILayout.Height(256));
            return texture;
        }

        #endregion

        #region AudioClip no
        
        public static AudioClip EUIMusic(this AudioClip clip)
        {
            clip = (AudioClip)EditorGUILayout.ObjectField(clip, typeof(AudioClip), true);
            //GUILayout.Label(clip, GUILayout.Width(512), GUILayout.Height(256));
            return clip;
        }

        public static AudioClip EUIMusic(this string name, AudioClip clip)
        {
            GUILayout.BeginHorizontal();
            name.EditorTextStyle();
            clip = (AudioClip)EditorGUILayout.ObjectField(clip, typeof(AudioClip), true);
            GUILayout.EndHorizontal();

            return clip;
        }

        #endregion

        #region GameObject no

        /// <summary>
        /// 获取物体
        /// </summary>
        /// <param name="name"></param>
        /// <param name="obj"></param>
        /// <returns></returns>
        public static GameObject EUIGameObject(this string name, GameObject obj)
        {
            GUILayout.BeginHorizontal();
            name.EditorTextStyle();
            obj = (GameObject)EditorGUILayout.ObjectField(obj, typeof(GameObject), true);
            GUILayout.EndHorizontal();

            return obj;
        }

        /// <summary>
        /// 获取物体
        /// </summary>
        /// <param name="name"></param>
        /// <param name="obj"></param>
        /// <param name="action"></param>
        public static void EUIGameObject(this string name, GameObject obj,Action<GameObject> action)
        {
            GUILayout.BeginHorizontal();
            name.EditorTextStyle();
            GameObject objTow = (GameObject)EditorGUILayout.ObjectField(obj, typeof(GameObject), true);
            GUILayout.EndHorizontal();

            if(action!= null)
            {
                action(objTow);
            }
        }

        #endregion

        #region T no

        public static T TField<T>(this T obj, bool allowSceneObjects = true)
            where T : UnityEngine.Object
        {
            return obj.TField(interval);
        }
        public static T TField<T>(this T obj, float flag, bool allowSceneObjects = true)
           where T : UnityEngine.Object
        {
            if (flag > 0)
            {
                flag.GSpace();
            }
            return (T)EditorGUILayout.ObjectField(obj, typeof(T), allowSceneObjects);
        }


        #endregion

        #region 分表 no

        /// <summary>
        /// 分表
        /// </summary>
        /// <param name="action"></param>
        public static void SubTable(Action action, bool flag = true)
        {
            if (flag)
            {
                DrawGuiDivider();
            }
            if (action != null)
            {
                action();
            }
        } 
        #endregion

        #region 一行

        /// <summary>
        /// 一行
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <typeparam name="Y"></typeparam>
        /// <typeparam name="K"></typeparam>
        /// <typeparam name="U"></typeparam>
        /// <param name="action"></param>
        /// <param name="t"></param>
        /// <param name="y"></param>
        /// <param name="k"></param>
        /// <param name="u"></param>
        /// <param name="style"></param>
        public static void EditorHorizontal<T,Y,K,U>(this Action<T, Y, K,U> action, T t, Y y, K k,U u, GUIStyle style)
        {
            EditorGUILayout.BeginHorizontal(style);
            if (action != null)
            {
                action(t, y, k,u);
            }
            EditorGUILayout.EndHorizontal();
        }

        /// <summary>
        /// 一行
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <typeparam name="Y"></typeparam>
        /// <typeparam name="K"></typeparam>
        /// <param name="action"></param>
        /// <param name="t"></param>
        /// <param name="y"></param>
        /// <param name="k"></param>
        /// <param name="style"></param>
        public static void EditorHorizontal<T,Y,K>(this Action<T,Y,K> action, T t,Y y,K k, GUIStyle style)
        {
            EditorGUILayout.BeginHorizontal(style);
            if (action != null)
            {
                action(t,y,k);
            }
            EditorGUILayout.EndHorizontal();
        }

        /// <summary>
        /// 一行
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <typeparam name="Y"></typeparam>
        /// <param name="action"></param>
        /// <param name="t"></param>
        /// <param name="y"></param>
        /// <param name="style"></param>
        public static void EditorHorizontal<T,Y>(this Action<T,Y> action, T t,Y y, GUIStyle style)
        {
            EditorGUILayout.BeginHorizontal(style);
            if (action != null)
            {
                action(t,y);
            }
            EditorGUILayout.EndHorizontal();
        }

        /// <summary>
        /// 一行
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="action"></param>
        /// <param name="t"></param>
        /// <param name="style"></param>
        public static void EditorHorizontal<T>(this Action<T> action, T t, GUIStyle style)
        {
            EditorGUILayout.BeginHorizontal(style);
            if (action != null)
            {
                action(t);
            }
            EditorGUILayout.EndHorizontal();
        }

        /// <summary>
        /// 一行
        /// </summary>
        /// <param name="action"></param>
        /// <param name="style"></param>
        public static void EditorHorizontal(this Action action, GUIStyle style)
        {
            EditorGUILayout.BeginHorizontal(style);
            if (action != null)
            {
                action();
            }
            EditorGUILayout.EndHorizontal();
        }

        #endregion

        //FontSize = Height * 0.7f;(可设置)
        //{
        //  GUIStyle style;
        //  style.SetFontStyle(比例,高);
        //}

        /// <summary>
        /// 设置字体大小
        /// </summary>
        /// <param name="style"></param>
        /// <param name="proportion">倍数</param>
        /// <param name="height">高</param>
        public static void SetFontStyle(this GUIStyle style, float proportion = 0.7f, float height = -1)
        {
            if (height == -1)
            {
                height = style.fixedHeight;
            }
            style.fontSize = Mathf.FloorToInt(height * proportion);
        }

        #endregion

        #endregion

    }
}
