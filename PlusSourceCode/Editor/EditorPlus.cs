﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using GameUI.Base;
using UnityEditor;
using UnityEngine;

namespace Game.Plus.Editor
{
    public static class EditorPlus
    {

        public static void SetDirtyRect<OBJ>(this OBJ obj)
            where OBJ : UIBaseGameObject<RectTransform>
        {
            EditorUtility.SetDirty(obj.com);
        }
        public static void SetDirty<OBJ>(this OBJ obj)
           where OBJ : UIBaseGameObject<Transform>
        {
            EditorUtility.SetDirty(obj.com);
        }
        public static void SetDirty(this GameObject gobj)
        {
            EditorUtility.SetDirty(gobj);
        }
        public static void SetDirty(this RectTransform t)
        {
            EditorUtility.SetDirty(t);
        }
    }
}
