﻿using Game.Plus.Data;
using Game.Plus.GFile;
using Game.Plus.GIO;
using Game.Plus.GIO.Data;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEditor;
using UnityEngine;

public class FileDataEditor : EditorWindow
{
    public string path ;
    public string pathName = "Texture";
    public string pathDic = "Assets/Resources";
    private DirectoryPlus.FileType fileType = DirectoryPlus.FileType.csv;

    [MenuItem(EditorConst.EditorLabelNameConstManage.GetPathNameStr)]
    public static void ShowWindow()
    {
        FileDataEditor fileData = new FileDataEditor();
        fileData.Show();
    }

    void OnGUI()
    {
        pathDic = EditorGUILayout.TextField("外文件加路径", pathDic);
        pathName = EditorGUILayout.TextField("文件夹名称", pathName);

        fileType = (DirectoryPlus.FileType)EditorGUILayout.EnumPopup(fileType);

        if (GUILayout.Button("生成"))
        {
            path = $"{pathDic}/{pathName}";
            List < FileData > fileDatas = DirectoryPlus.GetTypes(path, fileType);

            List<List<string>> strss = new List<List<string>>();

            List<string> strsHead = new List<string>();

            strsHead.Add("AllPath");
            strsHead.Add("ResourcesPath");
            strsHead.Add("name");

            strss.Add(strsHead);

            for (int i = 0; i < fileDatas.Count; i++)
            {
                List<string> strs = new List<string>();
                
                string resourcesPath = GetResourcesPathByFileData(fileDatas[i]);
                
                strs.Add(fileDatas[i].path);
                strs.Add(resourcesPath);
                strs.Add(fileDatas[i].name);


                strss.Add(strs);
            }
            string pathDA = path + "/PathData"+ pathName;
            FileManage.SetCSV(pathDA , strss,DirectoryType.Assets);
            Debug.LogError(pathDA);
        }
    }
    public string GetResourcesPathByFileData(FileData fileDara)
    {
        string resourcesPath = "";
        string res = "Resources";
        bool flag = false;
        string[] strs2 = fileDara.path.Split('\\');
        List<string> listStr = new List<string>();
        for (int j = 0; j < strs2.Length - 1; j++)
        {
            if (flag)
            {
                listStr.Add(strs2[j]);
            }
            else
            {
                if (strs2[j] == res)
                {
                    flag = true;
                }
            }
        }
        listStr.Add(fileDara.name);
        for (int j = 0; j < listStr.Count; j++)
        {
            if (j != 0)
            {
                resourcesPath += "/";
            }
            resourcesPath += listStr[j];
        }
        return resourcesPath;
    }

}
