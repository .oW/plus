﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Game.Plus
{
    public static class BytePlus
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="f"></param>
        /// <param name="count"></param>
        /// <returns></returns>
        public static byte[] GetByte(float f, int count)
        {
            int n = (int)(f * IntPlus.Power(10, count));
            byte[] bytes = new byte[4];
            bytes[0] = (byte)(n >> 24);
            bytes[1] = (byte)(n >> 16);
            bytes[2] = (byte)(n >> 8);
            bytes[3] = (byte)(n >> 0);
            return bytes;
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="f"></param>
        /// <param name="count"></param>
        /// <returns></returns>
        public static byte[] GetByte(int n)
        {
            byte[] bytes = new byte[4];
            bytes[0] = (byte)(n >> 24);
            bytes[1] = (byte)(n >> 16);
            bytes[2] = (byte)(n >> 8);
            bytes[3] = (byte)(n >> 0);
            return bytes;
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="f"></param>
        /// <param name="count"></param>
        /// <returns></returns>
        public static float GetFloat(byte[] bytes, int count)
        {
            int no = 0;    //转的时候还是要先转成int类型的
            no = no | (bytes[0] & 0xff) << 24;          //向左偏移
            no = no | (bytes[1] & 0xff) << 16;
            no = no | (bytes[2] & 0xff) << 8;
            no = no | (bytes[3] & 0xff) << 0;
            return no * 1.0f / IntPlus.Power(10, count);
        }
        public static int GetInt(byte[] bytes)
        {
            int no = 0;    //转的时候还是要先转成int类型的
            no = no | (bytes[0] & 0xff) << 24;          //向左偏移
            no = no | (bytes[1] & 0xff) << 16;
            no = no | (bytes[2] & 0xff) << 8;
            no = no | (bytes[3] & 0xff) << 0;
            return no;
        }
    }
}
