﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Game.Plus
{
    public static class DictionaryPlus
    {
        public static void Add<K, V>(Dictionary<K, V> dic, K k, V v)
        {
            if (dic.ContainsKey(k))
            {
                dic[k] = v;
            }
            else
            {
                dic.Add(k, v);
            }
        }

        public static void DicInit<K, V>(ref Dictionary<K, V> dic)
        {
            if(dic == null)
            {
                dic = new Dictionary<K, V>();
            }
            else
            {
                dic.Clear();
            }

        }
    }
}
