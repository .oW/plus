﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Game.Plus
{
    public static class IntPlus
    {
        /// <summary>
        /// 幂运算
        /// </summary>
        /// <param name="n">底数</param>
        /// <param name="count">指数</param>
        /// <returns></returns>
        public static int Power(int n, int count)
        {
            int num = 1;
            for (int i = 1; i <= count; i++)
            {
                num *= n;
            }
            return num;
        }
    }
}
