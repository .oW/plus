﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Game.Plus.GSystem
{
    public static class ArrayPlus
    {
        /// <summary>
        /// 移除指定位置的数据
        /// 并返回新表
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="ts"></param>
        /// <param name="index"></param>
        /// <returns></returns>
        public static T[] RemoveAt<T>(T[] ts, int index)
        {
            List<T> lts = ArrayToList<T>(ts);
            lts.RemoveAt(index);
            T[] arr = lts.ToArray();
            lts.Clear();
            return arr;
        }

        /// <summary>
        /// 增加数据
        /// 并返回新表
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="ts"></param>
        /// <param name="t"></param>
        /// <returns></returns>
        public static T[] Add<T>(T[] ts,T t)
        {
            List<T> lts = ArrayToList<T>(ts);
            lts.Add(t);
            T[] arr = lts.ToArray();
            lts.Clear();
            return arr;
        }

        /// <summary>
        /// 数组 []
        /// 转
        /// 列表 list
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="ts"></param>
        /// <returns></returns>
        public static List<T> ArrayToList<T>(T[] ts)
        {
            List<T> lts = TestGetY<T>.arr;
            lts.AddRange(ts);
            return lts;
        }

        public static int IndexOf<T>(T[] ts,T t)
        {
            List<T> lts = ArrayToList<T>(ts);
            int index = lts.IndexOf(t);
            lts.Clear();
            return index;
        }

        public class TestGetY<T>
        {
            public static List<T> arr = new List<T>();
        }
    }

}
