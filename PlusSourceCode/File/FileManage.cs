﻿using Game.Plus.GIO;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Game.Plus.GFile
{
    public static class FileManage 
    {
        #region CSV
        public static void SetCSV(string path, List<List<string>> strs, DirectoryType type)
        {
            List<string> listStrs = new List<string>();
            for (int i = 0; i < strs.Count; i++)
            {
                string str = "";
                for (int j = 0; j < strs[i].Count; j++)
                {
                    if (j == 0)
                    {
                        str += strs[i][j];
                    }
                    else
                    {

                        str += ("," + strs[i][j]);
                    }
                }
                listStrs.Add(str);
            }
            SetCSV(path, listStrs, type);
        }
        public static void SetCSV(string path, List<string> strs, DirectoryType type)
        {
            string str = "";
            for (int i = 0; i < strs.Count; i++)
            {
                if (i == 0)
                {
                    str += strs[i];
                }
                else
                {

                    str += ("\r\n" + strs[i]);
                }
            }
            SetCSVBy(path, str, type);
        }
        public static void SetCSVBy(string path, string strs, DirectoryType type)
        {
            switch (type)
            {
                case DirectoryType.Assets:
                    FilePlus.WriterAssets($"{path}.csv", false, strs);
                    break;
                case DirectoryType.Resources:
                    FilePlus.WriterResources(path, false, strs);
                    break;
                default:
                    break;
            }
        }

        public static void GetCSV(string path, ref List<string> listStr, DirectoryType type)
        {
            switch (type)
            {
                case DirectoryType.Assets:
                    listStr = FilePlus.ReaderAssets($"{path}.csv");
                    break;
                case DirectoryType.Resources:
                    listStr = FilePlus.ReaderResourcesText(path);
                    break;
                default:
                    break;
            }
        }

        /// <summary>
        /// 获取对应的CSV数组
        /// </summary>
        /// <param name="path"></param>
        /// <param name="strl"></param>
        public static void GetCSV(string path, ref List<List<string>> strl, DirectoryType type)
        {
            List<string> str = new List<string>();
            GetCSV(path, ref str, type);
            strl = new List<List<string>>();
            for (int i = 0; i < str.Count; i++)
            {
                string[] strs = str[i].Split(',');
                List<string> list = new List<string>();
                list.AddRange(strs);
                strl.Add(list);
            }
        }

        #endregion

        #region CS

        public static void SetCS(string path, string strs)
        {
            FilePlus.WriterAssets($"{path}.cs", false, strs);
        }

        #endregion

        #region Txt


        /// <summary>
        /// 获取对应的
        /// txt
        /// 集合
        /// </summary>
        /// <param name="path"></param>
        /// <param name="strl"></param>
        public static void GetTxt(string path, ref List<List<string>> strl, DirectoryType type)
        {
            List<string> str = new List<string>();
            GetTxt(path, ref str, type);
            strl = new List<List<string>>();
            for (int i = 0; i < str.Count; i++)
            {
                string[] strs = str[i].Split(',');
                List<string> list = new List<string>();
                list.AddRange(strs);
                strl.Add(list);
            }
        }

        private static void GetTxt(string path, ref List<string> listStr, DirectoryType type)
        {
            switch (type)
            {
                case DirectoryType.Assets:
                    listStr = FilePlus.ReaderAssets($"{path}.txt");
                    break;
                case DirectoryType.Resources:
                    listStr = FilePlus.ReaderResourcesText(path);
                    break;
                default:
                    break;
            }
        }

        #endregion

    }
}
