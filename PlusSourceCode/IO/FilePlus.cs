﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TextAsset = UnityEngine.TextAsset;
using Debug = UnityEngine.Debug;
using Text = System.Text;

namespace Game.Plus.GIO
{
    public static class FilePlus
    {

        public static void WriterAssets(string path, bool flag, string v)
        {
            
            Encoding code = new Text.UTF8Encoding(false);
            StreamWriter sw = null;
            {
                try
                {
                    using (sw = new StreamWriter(path, flag, code))
                    {
                        sw.Write(v);
                        sw.Flush();
                    }
                }
                catch (Exception e) { Console.WriteLine(e.ToString()); }
            }
            if (sw != null)
            {
                sw.Close();
                sw.Dispose();
            }
        }

        public static void WriterResources(string name, bool flag, string v)
        {
            Debug.LogError("暂时没写这方面的方法");
        }

        public static List<string> ReaderAssets(string path)
        {

            Encoding code = Encoding.GetEncoding("utf-8");
            List<string> strs = new List<string>();
            StreamReader sr = null;
            {
                try
                {
                    using (sr = new StreamReader(path, code))
                    {
                        while (true)
                        {
                            strs.Add(sr.ReadLine());

                            if (sr.EndOfStream)
                            {
                                break;
                            }
                        }
                    }
                }
                catch (Exception e) { Console.WriteLine(e.ToString()); }
            }
            if (sr != null)
            {
                sr.Close();
                sr.Dispose();
            }
            DeleteNullData(ref strs);
            return strs;
        }

        /// <summary>
        /// 读取文本
        /// </summary>
        /// <param name="name"></param>
        /// <returns></returns>
        public static List<string> ReaderResourcesText(string name)
        {
            List<string> strs = new List<string>();

            if(ResourcesConfigManager.GetIsExitRes(name))
            {
                TextAsset text = ResourceManager.Load<TextAsset>(name);
                string r = text.text;
                strs.AddRange(r.Split("\r\n".ToArray()));

                DeleteNullData(ref strs);
            }
            else
            {
                Debug.LogWarning(string.Format("文件:\"{0}\"不存在",name));
            }

            return strs;
        }

        /// <summary>
        /// 删除空数据
        /// </summary>
        /// <param name="str"></param>
        public static void DeleteNullData(ref List<string> strs)
        {
            for (int i = strs.Count - 1; i >= 0; i--)
            {
                if (strs[i] == "" || strs[i] == null || strs[i].StartsWith(","))
                {
                    strs.RemoveAt(i);
                }
                //else
                //{
                //    break;
                //}
            }
        }
    }
}
