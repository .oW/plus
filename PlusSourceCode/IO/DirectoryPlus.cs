﻿using Game.Plus.GIO.Data;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;

namespace Game.Plus.GIO
{
    public static class DirectoryPlus
    {
        public enum MainPath
        {
            Assets = 0,
        }
        public enum FileType
        {
            png = 0,
            csv = 1,
            mp3 = 2,
            ogg = 3,
            txt = 4,
            prefab = 5,
        }
        public static void AddDirectory(string path, params string[] strs)
        {
            if (!Directory.Exists(path))
            {
                Directory.CreateDirectory(path);
            }
            for (int i = 0; i < strs.Length; i++)
            {
                path += ($"/{strs[i]}");
                if (!Directory.Exists(path))
                {
                    Directory.CreateDirectory(path);
                }
            }
        }

        /// <summary>
        /// 获取规定的文件
        /// </summary>
        /// <param name="dType">文件夹名</param>
        /// <param name="fType">文件类型</param>
        public static List<FileData> GetTypes(DirectoryType dType, FileType fType, MainPath path)
        {
            return GetTypes(dType.ToString(), fType.ToString(), path.ToString());
        }
        /// <summary>
        /// 获取规定的文件
        /// </summary>
        /// <param name="dType">文件夹名</param>
        /// <param name="fType">文件类型</param>
        public static List<FileData> GetTypes(DirectoryType dType, FileType fType, string path)
        {
            return GetTypes(dType.ToString(), fType.ToString(), path);
        }
        /// <summary>
        /// 获取规定的文件
        /// </summary>
        /// <param name="dType">文件夹名</param>
        /// <param name="fType">文件类型</param>
        public static List<FileData> GetTypes(string dType, string fType, string path)
        {
            List<FileData> fileds = new List<FileData>();
            if (dType == "All")
            {
                //获取指定路径下面的所有资源文件  
                fileds.AddRange(GetTypes(path, fType));
            }
            else
            {
                List<string> paths = GetDirectorys(dType, path);
                for (int i = 0; i < paths.Count; i++)
                {
                    fileds.AddRange(GetTypes(paths[i], fType));
                }
                DirectoryInfo direction = new DirectoryInfo(path);
                DirectoryInfo[] dires = direction.GetDirectories("*", SearchOption.AllDirectories);

            }
            return fileds;
        }
        /// <summary>
        /// 获取规定的文件
        /// </summary>
        /// <param name="path"></param>
        /// <param name="fType"></param>
        /// <returns></returns>
        public static List<FileData> GetTypes(string path, FileType fType)
        {
            return GetTypes(path, fType.ToString());
        }
        /// <summary>
        /// 获取规定的文件
        /// </summary>
        /// <param name="path"></param>
        /// <param name="fType"></param>
        /// <returns></returns>
        public static List<FileData> GetTypes(string path, string fType)
        {
            List<FileData> fileds = new List<FileData>();
            if (Directory.Exists(path))
            {
                DirectoryInfo direction = new DirectoryInfo(path);
                FileInfo[] files = direction.GetFiles("*", SearchOption.AllDirectories);

                Debug.Log(string.Format("文件数量____{0}", files.Length));

                for (int i = 0; i < files.Length; i++)
                {
                    if (!files[i].Name.EndsWith($".{fType}"))
                    {
                        continue;
                    }
                    string name_f = files[i].Name;
                    int index = name_f.LastIndexOf(".");
                    string name = name_f.Substring(0, index);
                    fileds.Add(new FileData(name, fType, files[i].FullName));
                }
            }
            return fileds;
        }

        public static List<string> GetDirectorys(string dType, string path)
        {
            List<string> paths = new List<string>();

            DirectoryInfo direction = new DirectoryInfo(path);
            DirectoryInfo[] dires = direction.GetDirectories("*", SearchOption.AllDirectories);
            for (int i = 0; i < dires.Length; i++)
            {
                Debug.Log(dires[i].FullName);
                if (dires[i].Name != dType)
                {
                    continue;
                }
                paths.Add(dires[i].FullName);
            }
            return paths;
        }


        /// <summary>
        /// 创建文件夹
        /// </summary>
        /// <param name="path"></param>
        public static void CreateDirectory(string path)
        {
            if (!Directory.Exists(path))
            {
                Directory.CreateDirectory(path);
            }
        }

        /// <summary>
        /// 打开文件夹
        /// </summary>
        /// <param name="path"></param>
        public static void OpenFolder(string path)
        {
            if (!Directory.Exists(path))
            {
                Directory.CreateDirectory(path);
            }
            path = path.Replace("/", "\\");
            System.Diagnostics.Process.Start("explorer.exe", path);

        }
    }
}
