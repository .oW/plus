﻿
using Game.Plus;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;
using UnityEngine.UI;

namespace Game.Plus
{
    public static class CameraPlus
    {
        #region 参数

        private static Camera _main => Camera.main;

        private static Vector2 v2 => ScreenPlus.Instance.v2;

        #endregion

        #region 2DUI

        /// <summary>
        /// 通过UI在Canvas界面的位置来设置plan在3D场景中的位置
        /// </summary>
        public static void WorldByCanvas(float num, Image[] ims, GameObject[] pls, GameObject pl_main, Vector2 v2, Camera main = null)
        {
            List<Vector3> v3s_ = new List<Vector3>();
            List<Vector2> v2s_ = new List<Vector2>();
            for (int i = 0; i < ims.Length; i++)
            {
                v3s_.Add(ims[i].transform.localPosition);
                v2s_.Add(ims[i].rectTransform.sizeDelta);
            }
            WorldByCanvas(num, v3s_, v2s_, pls, pl_main, v2, main);
        }
        public static void WorldByCanvas(float num, Image[] ims, GameObject[] pls, GameObject pl_main, Camera main = null)
        {
            List<Vector3> v3s_ = new List<Vector3>();
            List<Vector2> v2s_ = new List<Vector2>();
            for (int i = 0; i < ims.Length; i++)
            {
                v3s_.Add(ims[i].transform.localPosition);
                v2s_.Add(ims[i].rectTransform.sizeDelta);
            }

            WorldByCanvas(num, v3s_, v2s_, pls, pl_main, v2, main);
        }

        /// <summary>
        /// 通过UI在Canvas界面的位置来设置plan在3D场景中的位置
        /// </summary>
        public static void WorldByCanvas(float num, GameObject imso, GameObject plso, Vector2 v2, Camera main = null)
        {
            Image[] ims = imso.GetComponentsInChildren<Image>();
            GameObject[] pls2 = plso.GetComponentsInChildren<GameObject>();
            GameObject[] ola = new GameObject[pls2.Length - 1];
            for (int i = 1; i < pls2.Length; i++)
            {
                ola[i - 1] = pls2[i];
            }
            if (main == null)
            {
                main = _main;
            }
            WorldByCanvas(num, ims, ola, plso, v2, main);
        }

        public static void WorldByCanvas(float num, GameObject imso, GameObject plso, Camera main = null)
        {
            Image[] ims = imso.GetComponentsInChildren<Image>();
            GameObject[] pls2 = plso.GetComponentsInChildren<GameObject>();
            GameObject[] ola = new GameObject[pls2.Length - 1];
            for (int i = 1; i < pls2.Length; i++)
            {
                ola[i - 1] = pls2[i];
            }
            if (main == null)
            {
                main = _main;
            }

            WorldByCanvas(num, ims, ola, plso, v2, main);
        }

        /// <summary>
        /// 只改变一个UI
        /// </summary>
        /// <param name="num"></param>
        /// <param name="imso"></param>
        /// <param name="plso"></param>
        /// <param name="main"></param>
        public static void WorldByCanvasPoint(float num, GameObject imso, GameObject plso, Vector2 v2, Camera main = null)
        {
            Vector3 v3s_ = imso.transform.localPosition;
            Vector2 v2s_ = imso.GetComponent<RectTransform>().sizeDelta;
            WorldByCanvasPoint(num, v3s_, v2s_, plso, v2);
        }

        /// <summary>
        /// 只改变一个UI
        /// </summary>
        /// <param name="num"></param>
        /// <param name="imso"></param>
        /// <param name="plso"></param>
        /// <param name="main"></param>
        public static void WorldByCanvasPoint(float num, GameObject imso, GameObject plso, Camera main = null)
        {
            Vector3 v3s_ = imso.transform.localPosition;
            Vector2 v2s_ = imso.GetComponent<RectTransform>().sizeDelta;

            WorldByCanvasPoint(num, v3s_, v2s_, plso, v2);
        }
        #region 2D to 3D

        public static GameObject ImageToPlane(Image image, float num, Vector2 v2)
        {
            GameObject plane = GameObject.CreatePrimitive(PrimitiveType.Plane);

            WorldByCanvasPoint(num, image.gameObject, plane, v2);

            //plane.transform.rotation = Quaternion.Slerp(plane.transform.rotation, Quaternion.LookRotation(_main.transform.position - plane.transform.position),0.01f);
            plane.transform.rotation = QuaternionPlus.AddTwo(_main.transform.rotation, new Quaternion(0.0f, 0.7f, -0.7f, 0.0f));

            plane.GetComponent<MeshRenderer>().material = new Material(Shader.Find("TextMeshPro/Sprite"));
            plane.GetComponent<MeshRenderer>().material.SetTexture(1, image.sprite.texture);

            return plane;
        }
        public static GameObject TextToText3D(Text text, float num, Vector2 v2, Material material)
        {
            GameObject text3d = new GameObject();
            TextMesh mesh = text3d.AddComponent<TextMesh>();
            mesh.font = text.font;
            mesh.text = text.text;

            WorldByCanvasPoint(num, text.gameObject, text3d, v2);
            //text3d.transform.rotation = QuaternionPlus.AddTwo(_main.transform.rotation, new Quaternion(0.0f, 0.7f, -0.7f, 0.0f));
            mesh.GetComponent<MeshRenderer>().material = material;

            return text3d;
        } 

        #endregion
        public static GameObject ImageToPlane(Image image, float num)
        {
            return ImageToPlane(image, num, v2);
        }

        #endregion

        #region 比例

        /// <summary>
        /// 根据比例获得来设置plan在3D场景中的位置
        /// </summary>
        /// <param name="num">与相机的距离</param>
        /// <param name="v3s_">位置比例</param>
        /// <param name="v2s_">缩放比例</param>
        /// <param name="pls">所有的plan</param>
        /// <param name="pl_main">plan的主节点</param>
        /// <param name="v2">窗口比例</param>
        /// <param name="main">相机</param>
        public static void WorldByCanvas(float num, List<Vector3> v3s_, List<Vector2> v2s_, GameObject[] pls, GameObject pl_main, Vector2 v2, Camera main = null)
        {
            if (main == null)
            {
                main = _main;
            }
            pl_main.transform.SetParent(main.transform);
            pl_main.transform.localPosition = main.transform.position * -1f;

            float x, y;
            GetXY(num, out x, out y, main);

            for (int i = 0; i < pls.Length; i++)
            {
                if (v3s_.Count <= i || v2s_.Count <= i)
                {
                    break;
                }
                WorldByCanvasPoint(num, v3s_[i], v2s_[i], pls[i], v2, x, y, main);
            }
        }

        /// <summary>
        /// 根据比例获得来设置plan在3D场景中的位置
        /// </summary>
        /// <param name="num">与相机的距离</param>
        /// <param name="v3s_">位置比例</param>
        /// <param name="v2s_">缩放比例</param>
        /// <param name="pls">所有的plan</param>
        /// <param name="pl_main">plan的主节点</param>
        /// <param name="v2">窗口比例</param>
        /// <param name="main">相机</param>
        public static void WorldByCanvas(float num, List<Vector3> v3s_, List<Vector2> v2s_, GameObject[] pls, GameObject pl_main)
        {
            pl_main.transform.SetParent(_main.transform);
            pl_main.transform.localPosition = _main.transform.position * -1f;

            float x, y;
            GetXY(num, out x, out y, _main);


            for (int i = 0; i < pls.Length; i++)
            {
                if (v3s_.Count <= i || v2s_.Count <= i)
                {
                    break;
                }
                WorldByCanvasPoint(num, v3s_[i], v2s_[i], pls[i], v2, x, y, _main);
            }
        }
        /// <summary>
        /// 根据比例获得来设置plan在3D场景中的位置及缩放
        /// </summary>
        /// <param name="num">与相机的距离</param>
        /// <param name="v3s">位置比例</param>
        /// <param name="v2">缩放比例</param>
        /// <param name="pl">当前的plan</param>
        /// <param name="v2_s">窗口比例</param>
        /// <param name="x">最大值x</param>
        /// <param name="y">最大值y</param>
        public static void WorldByCanvasPoint(float num, Vector3 v3s, Vector2 v2, GameObject pl, Vector2 v2_s, float x, float y, Camera main = null)
        {
            WorldByCanvasPosition(num, v3s, v2_s, pl, x, y, main);
            WorldByCanvasLocalScale(v2, v2_s, pl, x, y, main);
        }

        /// <summary>
        /// 根据比例获得来设置plan在3D场景中的位置及缩放
        /// </summary>
        /// <param name="num">与相机的距离</param>
        /// <param name="v3s">位置比例</param>
        /// <param name="v2">缩放比例</param>
        /// <param name="pl">当前的plan</param>
        /// <param name="v2_s">窗口比例</param>
        public static void WorldByCanvasPoint(float num, Vector3 v3s, Vector2 v2, GameObject pl, Vector2 v2_s, Camera main = null)
        {
            float x, y;
            GetXY(num, out x, out y, main);
            WorldByCanvasPosition(num, v3s, v2_s, pl, x, y, main);
            WorldByCanvasLocalScale(v2, v2_s, pl, x, y, main);
        }

        /// <summary>
        /// 根据比例获得来设置plan在3D场景中的位置
        /// </summary>
        /// <param name="num"></param>
        /// <param name="v3s"></param>
        /// <param name="v2_s"></param>
        /// <param name="pl"></param>
        /// <param name="x"></param>
        /// <param name="y"></param>
        /// <param name="main"></param>
        public static void WorldByCanvasPosition(float num, Vector3 v3s, Vector2 v2_s, GameObject pl, float x, float y, Camera main = null)
        {
            if (main == null)
            {
                main = _main;
            }
            Vector3 v3_ = v3s;
            Vector3 v3_p = main.transform.position;
            v3_p.x += v2_s.x == 0 ? 0 : v3_.x / v2_s.x * x;
            v3_p.y += v2_s.y == 0 ? 0 : v3_.y / v2_s.y * y;
            v3_p.z += num;
            pl.transform.localPosition = v3_p;
        }

        /// <summary>
        /// 根据比例获得来设置plan在3D场景中的缩放
        /// </summary>
        /// <param name="v2"></param>
        /// <param name="v2_s"></param>
        /// <param name="pl"></param>
        /// <param name="x"></param>
        /// <param name="y"></param>
        /// <param name="main"></param>
        public static void WorldByCanvasLocalScale(Vector2 v2, Vector2 v2_s, GameObject pl, float x, float y, Camera main = null)
        {
            if (main == null)
            {
                main = _main;
            }
            Vector2 v2_ = v2;
            Vector3 v3_s = pl.transform.localScale;
            v3_s.x = v2_s.x == 0 ? 0 : v2_.x / v2_s.x * x * 0.1f;
            v3_s.z = v2_s.y == 0 ? 0 : v2_.y / v2_s.y * y * 0.1f;
            pl.transform.localScale = v3_s;
        }

        /// <summary>
        /// 获取当前窗口的位置右上角的点
        /// </summary>
        /// <param name="num"></param>
        /// <param name="x"></param>
        /// <param name="y"></param>
        /// <param name="main"></param>
        public static void GetXY(float num, out float x, out float y, Camera main = null)
        {
            if (main == null)
            {
                main = _main;
            }
            var frustumHeight = 2.0 * num * Mathf.Tan(main.fieldOfView * 0.5f * Mathf.Deg2Rad);
            var distance = frustumHeight * 0.5 / Mathf.Tan(main.fieldOfView * 0.5f * Mathf.Deg2Rad);
            var Width = frustumHeight * main.aspect;
            var Height = Width / main.aspect;
            x = (float)Width;
            y = (float)Height;
        }
        #endregion
    }
}
