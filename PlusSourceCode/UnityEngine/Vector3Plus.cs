﻿
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;

namespace Game.Plus
{

    public static class Vector3Plus
    {

        /// <summary>
        /// 求和全为正数
        /// </summary>
        /// <param name="v3"></param>
        /// <returns></returns>
        public static float SumU(Vector3 v3)
        {
            float sum = 0;

            sum = FloatPlus.ToU(v3.x) + FloatPlus.ToU(v3.y) + FloatPlus.ToU(v3.z);

            return sum;
        }

        /// <summary>
        /// *
        /// </summary>
        /// <param name="v3"></param>
        /// <param name="f"></param>
        /// <returns></returns>
        public static Vector3 Multiply(Vector3 v3, float f)
        {
            Vector3 v3_ = new Vector3();

            v3_.x = v3.x * f;
            v3_.y = v3.y * f;
            v3_.z = v3.z * f;

            return v3_;
        }

        /// <summary>
        /// 插值范围
        /// </summary>
        /// <param name="now"></param>
        /// <param name="max"></param>
        /// <param name=""></param>
        /// <returns></returns>
        public static int ChangeRange(ref Vector3 now, Vector3 max, Vector3 v3_c)
        {
            int flag = 0;

            flag += FloatPlus.ChangeRange(ref now.x, max.x, v3_c.x);
            flag += FloatPlus.ChangeRange(ref now.y, max.y, v3_c.y);
            flag += FloatPlus.ChangeRange(ref now.z, max.z, v3_c.z);

            return flag;
        }

        public static List<Vector3> GetRange(List<Vector3> now, List<Vector3> max, float time_c)
        {
            List<Vector3> v3s_l = new List<Vector3>();
            for (int i = 0; i < max.Count; i++)
            {
                Vector3 v3_c = (max[i] - now[i]);
                v3_c.x *= time_c;
                v3_c.y *= time_c;
                v3_c.z *= time_c;
                v3s_l.Add(v3_c);
            }
            return v3s_l;
        }

        public static Vector3 GetRange(Vector3 now, Vector3 max, float time_c)
        {
            Vector3 v3_c = (max - now);
            v3_c.x *= time_c;
            v3_c.y *= time_c;
            v3_c.z *= time_c;
            return v3_c;
        }

        public static void GetRangeByCamera(Vector3 _max, Camera cam, out Vector3 max, out Vector3 now, out Vector3 c, float time_c)
        {
            max = _max;
            now = cam.transform.position;
            c = GetRange(now, max, time_c);
        }
        public static void GetRangeByGameObject(Vector3 _max, GameObject obj, out Vector3 max, out Vector3 now, out Vector3 c, float time_c)
        {
            max = _max;
            now = obj.transform.position;
            c = GetRange(now, max, time_c);
        }
        public static void GetRangeByGameObjectL(Vector3 _max, Vector3 v3, out Vector3 max, out Vector3 now, out Vector3 c, float time_c)
        {
            max = _max;
            now = v3;
            c = GetRange(now, max, time_c);
        }
        public static IEnumerator Move(Vector3 max, GameObject nowobj, float t_c, float t_max, float t_now)
        {
            Vector3 now = nowobj.transform.localPosition;
            Vector3 c = GetRange(now, max, t_c);
            float f_time_flag = 0;
            while (true)
            {
                ChangeRange(ref now, max, c);
                nowobj.transform.localPosition = now;

                if (f_time_flag >= t_max)
                {
                    break;
                }
                f_time_flag += t_now;
                yield return null;
            }
            yield return null;
        }
    }

}