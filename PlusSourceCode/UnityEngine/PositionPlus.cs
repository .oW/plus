﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;
using UnityEngine.UI;

namespace Game.Plus
{
    public static class PositionPlus
    {
        #region 坐标转换

        /// <summary>
        /// 世界转本地
        /// </summary>
        /// <param name="transform"></param>
        public static Vector3 LocalByWorld(Transform transform)
        {
            return transform.TransformVector(transform.position);
        }

        /// <summary>
        /// 本地转世界
        /// </summary>
        /// <param name="transform"></param>
        public static Vector3 WorldByLocal(Transform transform)
        {
            return transform.TransformPoint(transform.localPosition);
        }

        /// <summary>
        /// 屏幕坐标转世界坐标
        /// </summary>
        public static Vector3 WorldByScreen(Vector3 mousepos, Transform targetTransform)
        {
            //先计算相机到目标的向量
            Vector3 dir = targetTransform.position - Camera.main.transform.position;
            //计算投影
            Vector3 normardir = Vector3.Project(dir, Camera.main.transform.forward);
            //计算是节点，需要知道处置屏幕的投影距离
            Vector3 worldpos = Camera.main.ScreenToWorldPoint(new Vector3(mousepos.x, mousepos.y, normardir.magnitude));
            return worldpos;
        }

        /// <summary>
        /// 世界坐标转屏幕坐标
        /// </summary>
        /// <param name="tf"></param>
        /// <returns></returns>
        public static Vector3 ScreenByWorld(Transform tf)
        {
            return Camera.main.WorldToScreenPoint(tf.position);
        }

        /// <summary>
        /// 世界转视口
        /// </summary>
        /// <param name="tf"></param>
        /// <returns></returns>
        public static Vector3 WorldByViewport(Transform tf)
        {
            return Camera.main.ViewportToScreenPoint(tf.position);
        }

        /// <summary>
        /// 视口转世界
        /// </summary>
        /// <param name="tf"></param>
        /// <returns></returns>
        public static Vector3 ViewportByWorld(Transform tf)
        {
            return Camera.main.WorldToViewportPoint(tf.position);
        }


        #endregion
    }
}
