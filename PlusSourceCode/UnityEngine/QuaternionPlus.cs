﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;

namespace Game.Plus
{
    public static class QuaternionPlus
    {
        public static Quaternion zero = new Quaternion(0, 0, 0, 0);
        public static Quaternion Y180 = new Quaternion(0, 1, 0, 0);
        public static Quaternion R180 = new Quaternion(0, -1, 0, 0);

        /// <summary>
        /// +
        /// </summary>
        /// <param name="q4_1"></param>
        /// <param name="q4_2"></param>
        /// <returns></returns>
        public static Quaternion Add(Quaternion q4_1, Quaternion q4_2)
        {
            Quaternion q4 = new Quaternion();
            q4.x = q4_1.x + q4_2.x;
            q4.y = q4_1.y + q4_2.y;
            q4.z = q4_1.z + q4_2.z;
            return q4;
        }

        public static Quaternion AddTwo(Quaternion q4_1, Quaternion q4_2)
        {
            Quaternion q4 = new Quaternion();
            q4.x = (q4_1.x + q4_2.x) % 1;
            q4.y = (q4_1.y + q4_2.y) % 1;
            q4.z = (q4_1.z + q4_2.z) % 1;
            q4.w = (q4_1.w + q4_2.w) % 1;
            return q4;
        }

        /// <summary>
        /// -
        /// </summary>
        /// <param name="q4_1"></param>
        /// <param name="q4_2"></param>
        /// <returns></returns>
        public static Quaternion Subtract(Quaternion q4_1, Quaternion q4_2)
        {
            Quaternion q4 = new Quaternion();
            q4.x = q4_1.x - q4_2.x;
            q4.y = q4_1.y - q4_2.y;
            q4.z = q4_1.z - q4_2.z;
            return q4;
        }
        /// <summary>
        /// *
        /// </summary>
        /// <param name="q4_1"></param>
        /// <param name="q4_2"></param>
        /// <returns></returns>
        public static Quaternion Multiply(Quaternion q4_1, Quaternion q4_2)
        {
            Quaternion q4 = new Quaternion();
            q4.x = q4_1.x * q4_2.x;
            q4.y = q4_1.y * q4_2.y;
            q4.z = q4_1.z * q4_2.z;
            return q4;
        }
        /// <summary>
        /// *
        /// </summary>
        /// <param name="q4_1"></param>
        /// <param name="f"></param>
        /// <returns></returns>
        public static Quaternion Multiply(Quaternion q4_1, float f)
        {
            Quaternion q4 = new Quaternion();
            q4.x = q4_1.x * f;
            q4.y = q4_1.y * f;
            q4.z = q4_1.z * f;
            return q4;
        }
        /// <summary>
        /// /
        /// </summary>
        /// <param name="q4_1"></param>
        /// <param name="q4_2"></param>
        /// <returns></returns>
        public static Quaternion Divide(Quaternion q4_1, Quaternion q4_2)
        {
            Quaternion q4 = new Quaternion();
            q4.x = q4_1.x / q4_2.x;
            q4.y = q4_1.y / q4_2.y;
            q4.z = q4_1.z / q4_2.z;
            return q4;
        }
        /// <summary>
        /// /
        /// </summary>
        /// <param name="q4_1"></param>
        /// <param name="q4_2"></param>
        /// <returns></returns>
        public static Quaternion Divide(Quaternion q4_1, float q4_2)
        {
            Quaternion q4 = new Quaternion();
            q4.x = q4_1.x / q4_2;
            q4.y = q4_1.y / q4_2;
            q4.z = q4_1.z / q4_2;
            return q4;
        }

        public static int ChangeRange(ref Quaternion now, Quaternion max, Quaternion q4_c)
        {
            int flag = 0;

            flag += FloatPlus.ChangeRange(ref now.x, max.x, q4_c.x);
            flag += FloatPlus.ChangeRange(ref now.y, max.y, q4_c.y);
            flag += FloatPlus.ChangeRange(ref now.z, max.z, q4_c.z);

            return flag;
        }

        public static Quaternion GetRange(Quaternion now, Quaternion max, float time_c)
        {
            Quaternion c = Multiply(Subtract(max, now), time_c);
            return c;
        }
        public static void GetRangeByCamera(Quaternion _max, Camera cam, out Quaternion max, out Quaternion now, out Quaternion c, float time_c)
        {
            max = _max;
            now = cam.transform.rotation;
            c = GetRange(now, max, time_c);
        }
        public static void GetRangeByGameObject(Quaternion _max, GameObject cam, out Quaternion max, out Quaternion now, out Quaternion c, float time_c)
        {
            max = _max;
            now = cam.transform.rotation;
            c = GetRange(now, max, time_c);
        }
        public static void GetRangeByGameObjectL(Quaternion _max, GameObject cam, out Quaternion max, out Quaternion now, out Quaternion c, float time_c)
        {
            max = _max;
            now = cam.transform.localRotation;
            c = GetRange(now, max, time_c);
        }

    }
}
