﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;
using UnityEngine.UI;

namespace Game.Plus
{
    public static class TransformPlus
    {
        public static void Exchange(ref GameObject g_1, ref GameObject g_2)
        {
            Transform tf_1 = g_1.transform;
            Transform tf_2 = g_2.transform;
            Change2(ref g_1, tf_2);
            Change2(ref g_2, tf_1);
        }
        public static void Change(ref GameObject g, Transform tf)
        {
            g.transform.position = tf.position;
            Quaternion q4 = tf.rotation;
            q4.w = g.transform.rotation.w;
            g.transform.rotation = q4;
            g.transform.localScale = tf.localScale;
        }
        public static void Change2(ref GameObject g, Transform tf)
        {
            g.transform.position = tf.position;
        }

        public static T asC<T>(this Transform transform)
            where T : Component
        {
            return transform.GetComponent<T>();
        }

        /// <summary>
        /// 控制隐藏
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="t"></param>
        public static void SetActiveF<T>(this T t)
            where T : Component
        {
            //gameObject.activeSelf 不依赖父节点
            //gameObject.activeInHierarchy 依赖父节点

            if (t.gameObject.activeSelf)
            {
                t.gameObject.SetActive(false);
            }
        }

        /// <summary>
        /// 控制显示
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="t"></param>
        public static void SetActiveT<T>(this T t)
            where T : Component
        {
            //gameObject.activeSelf 不依赖父节点
            //gameObject.activeInHierarchy 依赖父节点
            if (!t.gameObject.activeSelf)
            {
                t.gameObject.SetActive(true);
            }
        }
        /// <summary>
        /// 控制显示隐藏
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="t"></param>
        public static void SetActive<T>(this T t,bool flag)
            where T : Component
        {
            if (flag)
            {
                t.SetActiveT();
            }
            else
            {
                t.SetActiveF();
            }
        }

        /// <summary>
        /// 是否隐藏
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="t"></param>
        public static bool IsHide<T>(this T t)
            where T : Component
        {
            return !t.gameObject.activeInHierarchy;
        }

    }
}
