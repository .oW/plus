﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;

namespace Game.Plus
{
    /// <summary>
    /// 
    /// </summary>
    public enum Edge
    {
        /// <summary>
        /// 左边
        /// </summary>
        Left = 0,
        /// <summary>
        /// 右边
        /// </summary>
        Right = 1,
        /// <summary>
        /// 上边
        /// </summary>
        Top = 2,
        /// <summary>
        /// 下边
        /// </summary>
        Bottom = 3,
        /// <summary>
        /// 左上
        /// </summary>
        LeftAndTop = 4,
        /// <summary>
        /// 左下
        /// </summary>
        LeftAndBottom = 5,
        /// <summary>
        /// 右上
        /// </summary>
        RightAndTop = 6,
        /// <summary>
        /// 右下
        /// </summary>
        RightAndBottom = 7,
        /// <summary>
        /// 自由
        /// </summary>
        All = 8
    }
    public static class RectTransformPlus
    {
        /// <summary>
        /// 设置锚点
        /// </summary>
        /// <param name="im"></param>
        /// <param name="edge"></param>
        public static void SetAnchorPoint(RectTransform rt, Edge edge,float size = 0)
        {
            switch (edge)
            {
                case Edge.Left:
                    rt.SetInsetAndSizeFromParentEdge(RectTransform.Edge.Left, 0,size);
                    break;
                case Edge.Right:
                    rt.SetInsetAndSizeFromParentEdge(RectTransform.Edge.Right, 0,size);
                    break;
                case Edge.Top:
                    rt.SetInsetAndSizeFromParentEdge(RectTransform.Edge.Top, 0,size);
                    break;
                case Edge.Bottom:
                    rt.SetInsetAndSizeFromParentEdge(RectTransform.Edge.Bottom, 0,size);
                    break;
                case Edge.LeftAndTop:
                    rt.SetInsetAndSizeFromParentEdge(RectTransform.Edge.Left, 0,size);
                    rt.SetInsetAndSizeFromParentEdge(RectTransform.Edge.Top, 0,size);
                    break;
                case Edge.LeftAndBottom:
                    rt.SetInsetAndSizeFromParentEdge(RectTransform.Edge.Left, 0,size);
                    rt.SetInsetAndSizeFromParentEdge(RectTransform.Edge.Bottom, 0,size);
                    break;
                case Edge.RightAndTop:
                    rt.SetInsetAndSizeFromParentEdge(RectTransform.Edge.Right, 0,size);
                    rt.SetInsetAndSizeFromParentEdge(RectTransform.Edge.Top, 0,size);
                    break;
                case Edge.RightAndBottom:
                    rt.SetInsetAndSizeFromParentEdge(RectTransform.Edge.Right, 0,size);
                    rt.SetInsetAndSizeFromParentEdge(RectTransform.Edge.Bottom, 0,size);
                    break;
                case Edge.All:
                    rt.SetInsetAndSizeFromParentEdge(RectTransform.Edge.Left, 0,size);
                    rt.SetInsetAndSizeFromParentEdge(RectTransform.Edge.Top, 0,size);
                    rt.anchorMin = Vector2.zero;
                    rt.anchorMax = Vector2.one;
                    break;
                default:
                    break;
            }
        }

        /// <summary>
        /// 获取UI的GameObject节点
        /// </summary>
        /// <returns></returns>
        public static GameObject GetGameObjectUI()
        {
            GameObject objUI = new GameObject();
            RectTransform rtf = objUI.gameObject.AddComponent<RectTransform>();
            return objUI;
        }

        /// <summary>
        /// 获取UI的GameObject节点
        /// </summary>
        /// <param name="name"></param>
        /// <param name="tr"></param>
        /// <returns></returns>
        public static GameObject GetGameObjectUI(string name,Transform tr)
        {
            GameObject objUI = GetGameObjectUI();
            objUI.name = name;
            objUI.transform.SetParent(tr);
            objUI.transform.localPosition = Vector3.zero;
            return objUI;
        }

        /// <summary>
        /// 获取UI的RectTransform
        /// </summary>
        /// <returns></returns>
        public static RectTransform GetGameUI(GameObject obj)
        {
            RectTransform rtf = obj.GetComponent<RectTransform>();
            if(rtf == null)
            {
                rtf = obj.gameObject.AddComponent<RectTransform>();
            }
            return rtf;
        }

        /// <summary>
        /// 设置大小
        /// </summary>
        public static void SetSize(this RectTransform rtr,Vector2 v2)
        {
            rtr.SetSizeWithCurrentAnchors(RectTransform.Axis.Horizontal, v2.x);
            rtr.SetSizeWithCurrentAnchors(RectTransform.Axis.Vertical, v2.y);
        }
    }
}
