﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;
using UnityEngine.UI;

namespace Game.Plus
{
    /// <summary>
    /// 创建Image
    /// </summary>
    public static class ImagePlus
    {
        /// <summary>
        /// 动态获取Image组件
        /// </summary>
        /// <param name="tr"></param>
        /// <param name="name"></param>
        /// <param name="sp"></param>
        /// <returns></returns>
        public static Image GetImage(Transform tr, string name, Sprite sp)
        {
            Image im = GetImage(tr, name);
            im.sprite = sp;
            return im;
        }

        /// <summary>
        /// 动态获取Image组件
        /// </summary>
        /// <param name="tr"></param>
        /// <param name="name"></param>
        /// <param name="sp"></param>
        /// <returns></returns>
        public static Image GetImage(Transform tr, string name, Color col)
        {
            Image im = GetImage(tr, name);
            im.color = col;
            return im;
        }

        /// <summary>
        /// 动态获取Image组件
        /// </summary>
        /// <param name="tr"></param>
        /// <param name="name"></param>
        /// <param name="sp"></param>
        /// <returns></returns>
        public static Image GetImage(Transform parent, string name)
        {
            Image im = GetImage();
            im.name = name;
            im.transform.SetParent(parent);
            im.transform.localPosition = Vector3.zero;
            return im;
        }

        /// <summary>
        /// 动态获取Image组件
        /// </summary>
        /// <param name="tr"></param>
        /// <param name="name"></param>
        /// <param name="sp"></param>
        /// <returns></returns>
        public static Image GetImage()
        {
            GameObject obj = new GameObject();
            obj.name = "Image";
            Image im = obj.AddComponent<Image>();
            return im;
        }

        /// <summary>
        /// 设置锚点
        /// </summary>
        /// <param name="im"></param>
        /// <param name="edge"></param>
        public static void SetAnchorPoint(Image rt, Edge edge)
        {
            RectTransformPlus.SetAnchorPoint(rt.GetComponent<RectTransform>(), edge);
        }

        
    }
}
