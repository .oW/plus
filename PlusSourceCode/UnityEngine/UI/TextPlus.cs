﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;
using UnityEngine.UI;


namespace Game.Plus
{
    public static class TextPlus
    {
        /// <summary>
        /// 获取Text组件
        /// </summary>
        /// <param name="trf"></param>
        /// <param name="txt_"></param>
        /// <returns></returns>
        public static Text GetText(Transform trf, string txt_)
        {
            GameObject ga_obj = new GameObject();
            ga_obj.name = "Text";
            ga_obj.transform.SetParent(trf);
            ga_obj.transform.localScale = Vector3.one;
            Text txt = ga_obj.AddComponent<Text>();
            txt.text = txt_;
            txt.font = Font.CreateDynamicFontFromOSFont("Arial", 14);
            txt.color = Color.black;
            txt.alignment = TextAnchor.MiddleCenter;
            return txt;
        }

        /// <summary>
        /// 获取艺术字
        /// </summary>
        /// <param name="trf"></param>
        /// <param name="txt"></param>
        /// <returns></returns>
        public static Text GetArtText(Transform trf,string txt = "")
        {
            Text text =  GetText(trf, txt);
            text.font = ResourceManager.Load<Font>("ArtCrit_font");
            text.material = ResourceManager.Load<Material>("ArtCrit_mat");

            return text;
        }
    }
}
