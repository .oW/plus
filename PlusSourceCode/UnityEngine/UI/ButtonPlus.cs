﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

namespace Game.Plus
{
    public static class ButtonPlus
    {
        /// <summary>
        /// 获取Button
        /// </summary>
        /// <param name="but_name"></param>
        /// <param name="txt_"></param>
        /// <returns></returns>
        public static Button GetButton(string but_name, string txt_)
        {
            GameObject ga_obj = new GameObject();
            ga_obj.name = but_name;
            ga_obj.transform.localScale = Vector3.one;

            Button but = ga_obj.AddComponent<Button>();
            RectTransform rtf = ga_obj.AddComponent<RectTransform>();
            Image im = ga_obj.AddComponent<Image>();
            but.image = im;
            Text txt = TextPlus.GetText(ga_obj.transform, txt_);
            return but;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="but"></param>
        /// <param name="Parent"></param>
        /// <param name="po"></param>
        /// <param name="size"></param>
        /// <param name="sp"></param>
        public static void ChangeButton(Button but, Transform Parent, Vector2 po, Vector2 size, Sprite sp)
        {
            but.transform.SetParent(Parent);
            but.transform.localPosition = po;
            but.GetComponent<RectTransform>().sizeDelta = size;
            but.GetComponent<Image>().sprite = sp;
        }

        /// <summary>
        /// 保证方法只添加一次
        /// </summary>
        /// <param name="btn"></param>
        public static void AddListenerOne(this Button btn,UnityAction call)
        {
            //判断是否增加过
            if (btn.onClick.GetPersistentEventCount() <= 0)
            {
                //为防止出错做的保护措施
                btn.onClick.RemoveAllListeners();
                //增加
                btn.onClick.AddListener(call);
            }
        }
    }
}
