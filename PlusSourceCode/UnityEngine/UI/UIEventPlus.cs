﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Events;
using UnityEngine.EventSystems;

namespace Game.Plus
{
    public static class UIEventPlus
    {
        public static void AddOnCilck(GameObject obj, UnityAction ac)
        {
            if (ac == null || obj == null)
            {
                return;
            }
            EventTrigger etr = obj.gameObject.GetComponent<EventTrigger>();
            if (etr == null)
            {
                etr = obj.gameObject.AddComponent<EventTrigger>();
            }
            EventTrigger.Entry entry = new EventTrigger.Entry();
            entry.eventID = EventTriggerType.PointerClick;
            entry.callback.AddListener((BaseEventData data2) => ac());
            etr.triggers.Add(entry);
        }
        /// <summary>
        /// 添加事件
        /// </summary>
        /// <param name="obj">物体</param>
        /// <param name="ac">事件</param>
        /// <param name="eventType">事件类型</param>
        public static void AddEvent(this GameObject obj, UnityAction ac, EventTriggerType eventType)
        {
            if (ac == null || obj == null)
            {
                return;
            }
            EventTrigger etr = obj.gameObject.GetComponent<EventTrigger>();
            if (etr == null)
            {
                etr = obj.gameObject.AddComponent<EventTrigger>();
            }
            EventTrigger.Entry entry = new EventTrigger.Entry();
            entry.eventID = eventType;
            entry.callback.AddListener((BaseEventData data2) => ac());
            etr.triggers.Add(entry);
        }

        public static void ClearEvent(this GameObject obj)
        {
            if ( obj == null)
            {
                return;
            }
            EventTrigger etr = obj.gameObject.GetComponent<EventTrigger>();
            if (etr != null)
            {
                etr.triggers.Clear();
            }
        }

    }
}
