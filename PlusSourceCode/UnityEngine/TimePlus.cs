﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;

namespace Game.Plus
{
    public static class TimePlus
    {
        public static void GetRangeByCamera(float f_time, out float max, out float now, out float c)
        {
            now = Time.deltaTime;
            //now = 0.0333f;
            //now = (float)(BitConverter.ToInt32(bte, 3) / 10000.0); ;
            max = f_time / 1000;
            c = now / max;
        }
        public static void GetRange(float f_time, out float max, out float now, out float c)
        {

            now = Time.deltaTime;
            //now = 0.0333f;
            //now = (float)(BitConverter.ToInt32(bte, 3) / 10000.0); ;
            max = f_time / 1000;
            c = now / max;
        }
    }
}
