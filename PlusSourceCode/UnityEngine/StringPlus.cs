﻿using System;
using System.Collections.Generic;
using System.Linq;
#if UNITY_EDITOR
using UnityEditor;
#endif
using UnityEngine;

namespace Game.Plus
{
    public class StringPlus : Singleton<StringPlus>
    {
        #region Function

        /// <summary>
        /// 拼接路径
        /// </summary>
        /// <param name="strs"></param>
        /// <returns></returns>
        public static string Combine(params string[] strs)
        {
            string str = "";

            for (int i = 0; i < strs.Length; i++)
            {
                if (i != 0)
                {
                    str = string.Concat(str, System.IO.Path.DirectorySeparatorChar);
                }
                str = string.Concat(str, strs[i]);
            }

            return str;
        }
        /// <summary>
        /// 拆分路径
        /// </summary>
        /// <param name="path"></param>
        /// <returns></returns>
        public static string[] SplitPath(string path)
        {
            string[] strs = path.Split(System.IO.Path.DirectorySeparatorChar);
            return strs;
        }

        /// <summary>
        /// 转大写
        /// </summary>
        /// <param name="index">位置</param>
        /// <param name="count">数量</param>
        /// <param name="str">要转的字符串</param>
        /// <returns></returns>
        public static string ToUp(int index, int count, string str)
        {
            char[] ch = str.ToArray();
            int sum = 0;
            for (int i = index; i < ch.Length; i++)
            {
                if (sum >= count)
                {
                    break;
                }
                if (ch[i] >= 'a' && ch[i] <= 'z')
                {
                    ch[i] = (char)(('A' - 'a') + ch[i]);
                }
                ++sum;
            }
            str = "";
            for (int i = 0; i < ch.Length; i++)
            {
                str += ch[i].ToString();
            }
            return str;
        }

        /// <summary>
        /// 转小写
        /// </summary>
        /// <param name="index">位置</param>
        /// <param name="count">数量</param>
        /// <param name="str">要转的字符串</param>
        /// <returns></returns>
        public static string ToDown(int index, int count, string str)
        {
            char[] ch = str.ToArray();
            int sum = 0;
            for (int i = index; i < ch.Length; i++)
            {
                if (sum >= count)
                {
                    break;
                }
                if (ch[i] >= 'A' && ch[i] <= 'Z')
                {
                    ch[i] = (char)(('a' - 'A') + ch[i]);
                }
                ++sum;
            }
            str = "";
            for (int i = 0; i < ch.Length; i++)
            {
                str += ch[i].ToString();
            }
            return str;
        }

        /// <summary>
        /// 分割特定开始字符串和结束字符串包裹的字符串
        /// </summary>
        /// <param name="value">要分割的字符串</param>
        /// <param name="headSign">框 左边 的字符串</param>
        /// <param name="endSign">框 右边 的字符串</param>
        /// <returns></returns>
        public string[] SplitValueByBox(string value, string headSign, string endSign)
        {
            List<string> results = new List<string>();

            int indexNow = 0;

            int headEndCount = 0;
            int headIndexNow = 0;

            for (int i = 0; i < value.Length; i++)
            {
                if (indexNow == -1 || indexNow == value.Length)
                {
                    break;
                }
                int headIndex = value.IndexOf(headSign, indexNow);
                int endIndex = value.IndexOf(endSign, indexNow);

                if (headIndex == -1 && endIndex == -1)
                {
                    break;
                }
                if (endIndex > headIndex && headIndex != -1)
                {
                    if (headEndCount == 0)
                    {
                        headIndexNow = headIndex;
                    }
                    ++headEndCount;
                    indexNow = headIndex + headSign.Length;
                }
                else
                {
                    if (endIndex != -1)
                    {
                        if (headEndCount <= 0)
                        {
                            headEndCount = 0;
                        }
                        else
                        {
                            --headEndCount;
                            if (headEndCount == 0)
                            {
                                int headI = headIndexNow + headSign.Length;
                                int endI = endIndex;
                                string str = value.Substring(headI, endI - headI);
                                results.Add(str);
                            }
                        }
                        indexNow = endIndex + endSign.Length;
                    }
                    else
                    {
                        indexNow = -1;
                    }
                }
            }

            return results.ToArray();
        }

        #endregion

        #region File

        #region 基础属性

        public static Color ColorZero = new Color(0,0,0);
        public static Quaternion QuaternionZero = new Quaternion(0,0,0,0);

        #endregion

        #region 拼接 拆分

        //写入时 特殊类型 之间的分隔符
        private const string delimiterWriterField = "~";
        //写入时 集合 之间的分隔符
        private const string delimiterWriterList = "&";
        //写入时 字典 之间的分隔符
        private const string delimiterWriterDictionary = "|";

        //读取时 特殊类型 之间的分隔符
        private const char delimiterReaderField = '~';
        //读取时 集合 之间的分隔符
        private const char delimiterReaderList = '&';
        //读取时 字典 之间的分隔符
        private const char delimiterReaderDictionary = '|';

        public static string SpliceDelimiter(string delimiter, params object[] objs)
        {
            string str = "";

            for (int i = 0; i < objs.Length; i++)
            {
                if (i != 0)
                {
                    str += delimiter;
                }
                str += objs[i].ToString();
            }

            return str;
        }
        public static string[] SplitDelimiter(char delimiter, string str)
        {
            string[] strs = str.Split(delimiter);

            return strs;
        }

        public static string SpliceField(params object[] objs)
        {
            return SpliceDelimiter(delimiterWriterField, objs);
        }

        public static string[] SplitField(string str)
        {
            return SplitDelimiter(delimiterReaderField, str);
        }

        #endregion

        #region String

        public static string StringByString(string str)
        {
            return str;
        }
        #endregion

        #region Float

        public static float FloatByString(string str)
        {
            try
            {
                double d = DoubleByString(str) * 100000;
                return Convert.ToSingle(d.ToString().Split('.')[0]) * 0.00001f;
                //return float.Parse(str); ;
            }
            catch (Exception)
            {
                return 0;
            }
        }
        /// <summary>
        /// 有问题暂时不要用
        /// </summary>
        /// <param name="str"></param>
        /// <returns></returns>
        public static float FloatByString2(string str)
        {
            
            try
            {
                byte[] l_bytes = System.Text.Encoding.Default.GetBytes(str);
                int accum = 0;
                accum = accum | (l_bytes[0] & 0xff) << 0;
                accum = accum | (l_bytes[1] & 0xff) << 8;
                accum = accum | (l_bytes[2] & 0xff) << 16;
                accum = accum | (l_bytes[3] & 0xff) << 24;
                //return accum;
                return BitConverter.ToSingle(l_bytes, 0);
            }
            catch (Exception)
            {
                return 0;
            }
        }
        public static string StringByFloat(float f)
        {
            return f == 0 ? "" : f.ToString();
        }
        #endregion

        #region Double

        public static double DoubleByString(string str)
        {
            try
            {
                return Convert.ToDouble(str); ;
            }
            catch (Exception)
            {
                return 0;
            }
        }
        public static string StringByDouble(double d)
        {
            return d == 0 ? "" : d.ToString();
        }
        #endregion

        #region Int

        public static int IntByString(string str)
        {
            try
            {
                return int.Parse(str);
            }
            catch (Exception)
            {
                return default(int);
            }

        }
        public static string StringByInt(int i)
        {
            return i == default(int) ? "" :i.ToString();
        }

        #endregion

        #region UInt

        public static uint UIntByString(string str)
        {
            try
            {
                return uint.Parse(str);
            }
            catch (Exception)
            {
                return default(uint);
            }

        }
        public static string StringByUInt(uint i)
        {
            return i == default(uint) ? "" : i.ToString();
        }

        #endregion

        #region Bool

        public static bool BoolByString(string str)
        {
            try
            {
                return str == "" ? false :bool.Parse(str);
                //return int.Parse(str) == 1 ? true : false;
            }
            catch (Exception)
            {
                return false;
            }
        }
        public static string StringByBool(bool b)
        {
            return b.ToString();
            //return StringByInt(b ? 1 : 0);
        }

        #endregion

        #region Color

        public static Color ColorByString(string str)
        {
            string[] strs = SplitField(str);
            Color col = Color.white;
            if (strs.Length >= 4)
            {
                col.r = FloatByString(strs[0]);
                col.g = FloatByString(strs[1]);
                col.b = FloatByString(strs[2]);
                col.a = FloatByString(strs[3]);
            }

            return col;
        }
        public static string StringByColor(Color col)
        {
            string str = "";
            if (col != ColorZero)
            {
                str = SpliceField(col.r, col.g, col.b, col.a);
            }
            return str;
        }

        #endregion

        #region Vector2

        public Vector2 Vector2ByString(string str)
        {
            string[] strs = SplitField(str);
            Vector2 v2 = Vector2.zero;
            if (strs.Length >= 2)
            {
                v2.x = FloatByString(strs[0]);
                v2.y = FloatByString(strs[1]);
            }

            return v2;
        }
        public string StringByVector2(Vector2 v2)
        {
            string str = "";
            if (v2 != Vector2.zero)
            {
                str = SpliceField(v2.x, v2.y);
            }
            return str;
        }

        #endregion

        #region Vector3

        public Vector3 Vector3ByString(string str)
        {
            string[] strs = SplitField(str);
            Vector3 v3 = Vector3.zero;
            if (strs.Length >= 3)
            {
                v3.x = FloatByString(strs[0]);
                v3.y = FloatByString(strs[1]);
                v3.z = FloatByString(strs[2]);
            }

            return v3;
        }
        public string StringByVector3(Vector3 v3)
        {
            string str = "";
            if (v3 != Vector3.zero)
            {
                str = SpliceField(v3.x, v3.y, v3.z);
            }
            return str;
        }

        #endregion

        #region  Quaternion

        public Quaternion QuaternionByString(string str)
        {
            string[] strs = SplitField(str);
            Quaternion q4 = QuaternionPlus.zero;
            if (strs.Length >= 4)
            {
                q4.x = FloatByString(strs[0]);
                q4.y = FloatByString(strs[1]);
                q4.z = FloatByString(strs[2]);
                q4.w = FloatByString(strs[3]);
            }

            return q4;
        }
        public string StringByQuaternion(Quaternion q4)
        {
            string str = "";
            if (q4 != QuaternionZero)
            {
                str = SpliceField(q4.x, q4.y, q4.z, q4.w);
            }
            return str;
        }

        #endregion

        #region  Sprite

        public Sprite SpriteByString(string str)
        {
            Texture2D tture = Resources.Load<Texture2D>(str);
            Sprite sp = Sprite.Create(tture, new Rect(0, 0, tture.width, tture.height), new Vector2(0.5f, 0.5f));
            return sp;
        }
        public string StringBySprite(Sprite sp)
        {
            string str = "";
#if UNITY_EDITOR
            str = AssetDatabase.GetAssetPath(sp);
#endif
            return str;
        }

        #endregion

        #region T

        public Dictionary<int, List<T>> DictionaryByString<T>(string str)
        {
            Dictionary<int, List<T>> ts = new Dictionary<int, List<T>>();
            if(str != null && str != "")
            {
                string[] strs = str.Split(delimiterReaderDictionary);
                if (strs.Length > 0)
                {
                    for (int i = 0; i < strs.Length; i++)
                    {
                        ts.Add(i, ListByString<T>(strs[i]));
                    }
                }
            }
            return ts;
        }
        public string StringByDictionary<T>(Dictionary<int, List<T>> ts)
        {
            string str = "";

            if(ts != null)
            {
                for (int i = 0; i < ts.Count; i++)
                {
                    if (i != 0)
                    {
                        str += delimiterWriterDictionary;
                    }
                    str += StringByList<T>(ts[i]);
                }
            }

            return str;
        }

        public List<T> ListByString<T>(string str)
        {
            List<T> ts = new List<T>();
            if(str != null && str != "")
            {
                string[] strs = str.Split(delimiterReaderList);
                if (strs.Length > 0)
                {
                    for (int i = 0; i < strs.Length; i++)
                    {
                        ts.Add(TByString<T>(strs[i]));
                    }
                }
            }
            return ts;
        }
        public string StringByList<T>(List<T> ts)
        {
            string str = "";
            if(ts != null)
            {
                for (int i = 0; i < ts.Count; i++)
                {
                    if (i != 0)
                    {
                        str += delimiterWriterList;
                    }
                    str += StringByT<T>(ts[i]);
                }
            }
            return str;
        }

        public T TByString<T>(string str)
        {
            T t = default(T);
            GetY<T> co = Tes.GetGetY<string, GetY<T>>(typeof(T).Name);
            if (co != null)
            {
                t = co(str);
            }
            return t;
        }
        public string StringByT<T>(T t)
        {
            string str = "";
            if (t!= null )
            {
                GetString<T> getstr = Tes.GetGetString<string, GetString<T>>(typeof(T).Name);
                if (getstr != null)
                {
                    str = getstr(t);
                }
            }
            return str;
        }

        private T GetT<T, Y>(Y y)
        {
            return (T)Convert.ChangeType(y, typeof(T));
        }

        private T GetT<T>(Type y)
        {
            return (T)Convert.ChangeType(y, typeof(T));
        }

        #endregion

        #region Test

        private GenericDataStorage tes = new GenericDataStorage();

        public GenericDataStorage Tes
        {
            get
            {
                if (tes == null)
                {
                    tes = new GenericDataStorage();
                }
                return tes;
            }
        }
        protected override void OnInit()
        {
            SetGetYAndString<string>(StringByString, StringByString);
            SetGetYAndString<int>(IntByString, StringByInt);
            SetGetYAndString<uint>(UIntByString, StringByUInt);
            SetGetYAndString<float>(FloatByString, StringByFloat);
            SetGetYAndString<double>(DoubleByString, StringByDouble);
            SetGetYAndString<bool>(BoolByString, StringByBool);
            SetGetYAndString<Vector2>(Vector2ByString, StringByVector2);
            SetGetYAndString<Vector3>(Vector3ByString, StringByVector3);
            SetGetYAndString<Quaternion>(QuaternionByString, StringByQuaternion);
            SetGetYAndString<Color>(ColorByString, StringByColor);
            SetGetYAndString<Sprite>(SpriteByString, StringBySprite);
        }

        /// <summary>
        /// 直接添加
        /// 通过字符串获取所要的类
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="t"></param>
        public void SetGetY<T>(GetY<T> t)
        {
            Tes.SetGetY<string, GetY<T>>(typeof(T).Name, t);
        }

        /// <summary>
        /// 直接添加
        /// 通过类获取所要的字符串
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="t"></param>
        public void SetGetString<T>(GetString<T> t)
        {
            Tes.SetGetString<string, GetString<T>>(typeof(T).Name, t);
        }

        /// <summary>
        /// 直接添加
        /// 通过字符串获取所要的类
        /// 通过类获取所要的字符串
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="t1"></param>
        /// <param name="t2"></param>
        public void SetGetYAndString<T>(GetY<T> t1, GetString<T> t2)
        {
            SetGetY<T>(t1);
            SetGetString<T>(t2);
        }

        #endregion

        #region 泛型储存数据

        public class GenericDataStorage
        {

            public void SetGetY<T, Y>(T t, Y y)
            {
                if (!TestGetY<T, Y>.tys.ContainsKey(t))
                {
                    TestGetY<T, Y>.tys.Add(t, y);
                }
            }
            public void SetGetString<T, Y>(T t, Y y)
            {
                if (!TestGetString<T, Y>.tys.ContainsKey(t))
                {
                    TestGetString<T, Y>.tys.Add(t, y);
                }
            }
            public Y GetGetY<T, Y>(T t)
            {
                if (TestGetY<T, Y>.tys.ContainsKey(t))
                {
                    return TestGetY<T, Y>.tys[t];
                }
                return default(Y);
            }
            public Y GetGetString<T, Y>(T t)
            {
                if (TestGetString<T, Y>.tys.ContainsKey(t))
                {
                    return TestGetString<T, Y>.tys[t];
                }
                return default(Y);
            }
        }

        public delegate T GetY<T>(string str);
        public delegate string GetString<T>(T t);
        internal class TestGetY<T, Y>
        {
            internal static Dictionary<T, Y> tys = new Dictionary<T, Y>();
        }
        internal class TestGetString<T, Y>
        {
            internal static Dictionary<T, Y> tys = new Dictionary<T, Y>();
        }
        #endregion

        #endregion

    }

}
