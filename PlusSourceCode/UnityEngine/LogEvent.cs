﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using System;

public class LogEvent : MonoBehaviour
{
#if !UNITY_EDITOR

    StringBuilder logBuilder = new StringBuilder();

    //是否开启
    bool openFlag;

    //画布大小
    Vector2 plonSize = Vector2.zero;

    void Awake()
    {
        Application.logMessageReceived += HandleLog;

        //Application.logMessageReceivedThreaded += HandleLog;
    }

    // Use this for initialization
    void Start()
    {
        openFlag = false;
    }

    // Update is called once per frame
    void Update()
    {
    }

    void OnGUI()
    {
        string butTxt = openFlag ? "关闭" : "打开";

        GUILayout.BeginHorizontal();
        if (GUILayout.Button(butTxt, Btn))
        {
            openFlag = !openFlag;
        }
        if (GUILayout.Button("清理", Btn))
        {
            ClearLog();
        }

        GUILayout.EndHorizontal();


        if (openFlag)
        {
            plonSize = GUILayout.BeginScrollView(plonSize);

            GUILayout.Label(logBuilder.ToString(), Txt);

            GUILayout.EndScrollView();
        }

    }

    void HandleLog(string condition, string stackTrace, LogType type)
    {
        
        if(string.IsNullOrEmpty(stackTrace))
        {
            return;
        }

        if (type == LogType.Error || type == LogType.Assert || type == LogType.Exception)
        {
            string message = string.Format("time = {3} \n type = {2}\n condition = {0} \n stackTrace = {1} \n ", condition, stackTrace, type, GetTime());
            SendLog(message);
        }
    }
    public string GetTime()
    {
        //获取当前时间
        float hour = DateTime.Now.Hour;
        float minute = DateTime.Now.Minute;
        float second = DateTime.Now.Second;
        //float year = DateTime.Now.Year;
        //float month = DateTime.Now.Month;
        //float day = DateTime.Now.Day;

        //格式化显示当前时间
        return string.Format("{0}:{1}:{2} "/* + "{3:D4}/{4:D2}/{5:D2}"*/, hour, minute, second/*, year, month, day*/);

    }

    /// <summary>
    /// 增加文本
    /// </summary>
    /// <param name="message"></param>
    void SendLog(string message)
    {
        logBuilder.Append(message);
    }

    /// <summary>
    /// 清空文本
    /// </summary>
    void ClearLog()
    {
        logBuilder.Clear();
    }


    #region Button 格式
    //Button One
    private GUIStyle _btn;
    public GUIStyle Btn
    {
        get
        {
            if (_btn == null)
            {
                _btn = ThinButtonStyle();
            }
            return _btn;
        }
    }
    public GUIStyle ThinButtonStyle(float Height = 64f, FontStyle style = FontStyle.Normal)
    {
        GUIStyle thinButton = new GUIStyle();/* (EditorStyles.toolbarButton);*/
        thinButton.fontStyle = style;
        thinButton.fixedHeight = Height;
        SetFontStyle(ref thinButton);


        return thinButton;
    }
    public void SetFontStyle(ref GUIStyle style, float proportion = 0.7f, float height = -1)
    {
        if (height == -1)
        {
            height = style.fixedHeight;
        }
        style.fontSize = Mathf.FloorToInt(height * proportion);
    }
    #endregion

    #region 文本 格式

    //Button One
    private GUIStyle _txt;
    public GUIStyle Txt
    {
        get
        {
            if (_txt == null)
            {
                _txt = ThinTextStyle();
            }
            return _txt;
        }
    }
    public GUIStyle ThinTextStyle(int Height = 32)
    {
        GUIStyle thinText = new GUIStyle();// (EditorStyles.textArea);
        thinText.fontSize = Height;
        thinText.normal.textColor = Color.red;

        return thinText;
    }

    #endregion
#else

    // Use this for initialization
    void Start()
    {
        
    }

#endif
}

