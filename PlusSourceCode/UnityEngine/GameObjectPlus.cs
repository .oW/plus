﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using GameUI.Base;
using UnityEngine;

namespace Game.Plus
{
    public static class GameObjectPlus
    {
        public static T GetChild<T>(this GameObject obj,int index)
        {
            return obj.transform.GetChild(index).GetComponent<T>();
        }
        public static GameObject GetChild(this GameObject obj, int index)
        {
            return obj.transform.GetChild(index).gameObject;
        }
        public static T GetChild<T, C>(this C c, int index)
            where C : Component
            where T : Component
        {
            return c.transform.GetChild(index).GetComponent<T>();
        }
        public static void GetChild<T, C>(this C c, out T t, int index)
            where C : Component
            where T : Component
        {
            t = c.transform.GetChild(index).GetComponent<T>();
        }
        public static T asC<T>(this GameObject GObj)
            where T : UnityEngine.Object
        {
            return GObj.GetComponent<T>();
        }
        public static T asC<T>(this UIBaseGameObject<Transform> GObj)
            where T : UnityEngine.Object
        {
            return GObj.com.gameObject.GetComponent<T>();
        }

        public static GameObject GetChild(this GameObject obj, string childName)
        {
            GameObject game = null;

            Queue<GameObject> objs = new Queue<GameObject>();
            objs.Enqueue(obj);

            while(objs.Count <= 0)
            {
                GameObject GObj = objs.Dequeue();
                for (int i = 0; i < GObj.transform.childCount; i++)
                {
                    GameObject objChile = GObj.GetChild(i);
                    if (objChile.name == childName)
                    {
                        game = objChile;
                        objs.Clear();
                        break;
                    }
                    else
                    {
                        objs.Enqueue(objChile);
                    }
                }
            }

            return game;
        }

        public static T GetUI<T>(this T t)
            where T : UIWindowBase
        {

            t = UIManager.GetUI<T>();
            if (t == null)
            {
                t = UIManager.GetHideUI<T>();
            }
            return t;
        }
    }
}
