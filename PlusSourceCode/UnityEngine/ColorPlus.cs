﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;


namespace Game.Plus
{
    public static class ColorPlus
    {
        public static Color Init(Color col_now, Color col_next, float time_f)
        {
            Color col = (col_next - col_now) * time_f;
            return col;
        }
        public static void ChangeRange(ref Color col_now, Color col_next, Color col_c)
        {
            FloatPlus.ChangeRange(ref col_now.r, col_next.r, col_c.r);
            FloatPlus.ChangeRange(ref col_now.g, col_next.g, col_c.g);
            FloatPlus.ChangeRange(ref col_now.b, col_next.b, col_c.b);
            FloatPlus.ChangeRange(ref col_now.a, col_next.a, col_c.a);
        }
    }
}
