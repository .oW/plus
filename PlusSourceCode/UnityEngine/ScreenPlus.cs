﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;

namespace Game.Plus
{
    public class ScreenPlus : Singleton<ScreenPlus>
    {
        private Vector2 _v2;
        public Vector2 v2 { get => _v2; }

        protected override void OnInit()
        {
            if (_v2 == null || _v2.x == 0 || _v2.y == 0)
            {
                _v2 = new Vector2(1920, 1080);
                //_v2 = new Vector2(Screen.width, Screen.height);
            }
        }
    }
}
