﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;

namespace Game.Plus
{
    public class MaterialPlus : Singleton<MaterialPlus>
    {
        #region Init

        GenericDataStorage gd = new GenericDataStorage();
        public GenericDataStorage Gd
        {
            get
            {
                if (gd == null)
                {
                    gd = new GenericDataStorage();
                }
                return gd;
            }
        }
        protected override void OnInit()
        {
            SetGetTAndSetT<float>(GetFloat, SetFloat);
            SetGetTAndSetT<int>(GetInt, SetInt);
            SetGetTAndSetT<Color>(GetColor, SetColor);
            SetGetTAndSetT<Color[]>(GetColorArray, SetColorArray);
            SetGetTAndSetT<float[]>(GetFloatArray, SetFloatArray);
            SetGetTAndSetT<Texture>(GetTexture, SetTexture);
            SetGetTAndSetT<Sprite>(GetSprite, SetSprite);
        }

        /**/

        #region 基础方法

        /// <summary>
        /// 直接添加
        /// get 方法
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="t"></param>
        public void SetGetT<T>(GetT_<T> t)
        {
            Gd.SetGetT<string, GetT_<T>>(typeof(T).Name, t);
        }

        /// <summary>
        /// 直接添加
        /// set 方法
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="t"></param>
        public void SetSetT<T>(SetT_<T> t)
        {
            Gd.SetSetT<string, SetT_<T>>(typeof(T).Name, t);
        }

        /// <summary>
        /// 直接添加
        /// get set 一起添加
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="t1"></param>
        /// <param name="t2"></param>
        public void SetGetTAndSetT<T>(GetT_<T> t1, SetT_<T> t2)
        {
            SetGetT<T>(t1);
            SetSetT<T>(t2);
        }


        public T GetT<T>(Material m, string name)
        {
            GetT_<T> sett = Gd.GetGetT<string, GetT_<T>>(typeof(T).Name);
            if (sett != null)
            {
                return sett(m, name);
            }
            return default(T);
        }
        public void SetT<T>(Material m, string name, T t)
        {
            SetT_<T> sett = Gd.GetSetT<string, SetT_<T>>(typeof(T).Name);
            if (sett != null)
            {
                sett(m, name, t);
            }
        }

        #endregion

        /**/

        #endregion

        /**/

        #region Sprite

        private void SetSprite(Material m, string name, Sprite s)
        {
            m.SetTexture(name, s.texture);
        }
        private Sprite GetSprite(Material m, string name)
        {
            Texture t = m.GetTexture(name);
            
            return Sprite.Create(t as Texture2D, new Rect(0, 0, t.width, t.height), new Vector2(0.5f, 0.5f));
        }

        #endregion

        /**/

        #region Texture

        private void SetTexture(Material m, string name, Texture t)
        {
            m.SetTexture(name, t);
        }
        private Texture GetTexture(Material m, string name)
        {
            return m.GetTexture(name);
        }

        #endregion

        /**/

        #region Float[]

        private void SetFloatArray(Material m, string name, float[] f)
        {
            m.SetFloatArray(name, f);
        }
        private float[] GetFloatArray(Material m, string name)
        {
            return m.GetFloatArray(name);
        }

        #endregion

        /**/

        #region Color[]

        private void SetColorArray(Material m, string name, Color[] c)
        {
            m.SetColorArray(name, c);
        }
        private Color[] GetColorArray(Material m, string name)
        {
            return m.GetColorArray(name);
        }

        #endregion

        /**/

        #region Color

        private void SetColor(Material m, string name, Color c)
        {
            m.SetColor(name, c);
        }
        private Color GetColor(Material m, string name)
        {
            return m.GetColor(name);
        }

        #endregion

        /**/

        #region Int

        private void SetInt(Material m, string name, int f)
        {
            m.SetInt(name, f);
        }
        private int GetInt(Material m, string name)
        {
            return m.GetInt(name);
        }

        #endregion

        /**/

        #region Float

        private void SetFloat(Material m, string name, float f)
        {
            m.SetFloat(name, f);
        }
        private float GetFloat(Material m, string name)
        {
            return m.GetFloat(name);
        }

        #endregion

        /**/

        #region 泛型储存数据

        public class GenericDataStorage
        {

            public void SetGetT<T, Y>(T t, Y y)
            {
                if (!TestGetT<T, Y>.tys.ContainsKey(t))
                {
                    TestGetT<T, Y>.tys.Add(t, y);
                }
            }
            public void SetSetT<T, Y>(T t, Y y)
            {
                if (!TestSetT<T, Y>.tys.ContainsKey(t))
                {
                    TestSetT<T, Y>.tys.Add(t, y);
                }
            }
            public Y GetGetT<T, Y>(T t)
            {
                if (TestGetT<T, Y>.tys.ContainsKey(t))
                {
                    return TestGetT<T, Y>.tys[t];
                }
                return default(Y);
            }
            public Y GetSetT<T, Y>(T t)
            {
                if (TestSetT<T, Y>.tys.ContainsKey(t))
                {
                    return TestSetT<T, Y>.tys[t];
                }
                return default(Y);
            }
        }

        public delegate T GetT_<T>(Material m, string name);
        public delegate void SetT_<T>(Material m, string name, T t);
        internal class TestGetT<T, Y>
        {
            internal static Dictionary<T, Y> tys = new Dictionary<T, Y>();
        }
        internal class TestSetT<T, Y>
        {
            internal static Dictionary<T, Y> tys = new Dictionary<T, Y>();
        }
        #endregion

    }
}
