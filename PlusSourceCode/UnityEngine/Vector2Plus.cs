﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;

namespace Game.Plus
{
    public static class Vector2Plus
    {
        public static Vector2 GetRange(Vector2 now, Vector2 max, float time_c)
        {

            Vector2 v2_c = (max - now);
            v2_c.x *= time_c;
            v2_c.y *= time_c;
            return v2_c;
        }
        public static int ChangeRange(ref Vector2 now, Vector2 max, Vector2 c)
        {
            int flag = 0;

            flag += FloatPlus.ChangeRange(ref now.x, max.x, c.x);
            flag += FloatPlus.ChangeRange(ref now.y, max.y, c.y);

            return flag;
        }
    }
}
