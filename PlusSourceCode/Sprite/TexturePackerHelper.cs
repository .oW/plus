﻿using Game.Plus.Data;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;

public class TexturePackerHelper
{
    private Dictionary<int, Data.client_atlasCSVData> m_atlasTableData;
    public void Init()
    {
        m_atlasTableData = DataBasePlus.GetDicData<Data.client_atlasCSVData>();
    }

    public Sprite GetSprite(int TemplateID)
    {
        if (!m_atlasTableData.ContainsKey(TemplateID))
        {
            Debug.LogError("图集数据读取失败  ID：" + TemplateID);
            return null;
        }
        var atlasName = Path.GetFileName(m_atlasTableData[TemplateID].Atlas_path);
        Sprite[] sprites = ResourceManager.loadSpriteAll(atlasName);

        Dictionary<string, Sprite> spritesDict = new Dictionary<string, Sprite>();

        foreach (Sprite sprite in sprites)
        {
            spritesDict.Add(sprite.name, sprite);
        }
        string iconName = m_atlasTableData[TemplateID].Icon_name;
        if (!spritesDict.ContainsKey(iconName))
        {
            Debug.LogError("图片配置信息有误" + m_atlasTableData[TemplateID].Icon_name);
            Debug.LogError("TemplateID:" + TemplateID);
        }
        Sprite s = spritesDict[iconName];

        if (s)
        {
            return s;
        }
        else
        {
            Debug.LogError("图片获取有误" + m_atlasTableData[TemplateID].Icon_name);
            return null;
        }
    }
}
