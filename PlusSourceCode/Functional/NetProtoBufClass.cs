﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Game.Plus.Functional
{
    public class NetProtoBufClass
    {


        /// <summary>  
        /// 获取一个命名空间下的所有类  
        /// </summary>  
        /// <param name="name"></param>  
        /// <returns></returns>  
        public static void GetTypes()
        {
            List<Type> gc = new List<Type>();
            List<Type> cg = new List<Type>();
            try
            {
                foreach (var item in System.Reflection.Assembly.GetExecutingAssembly().GetTypes())
                {
                    if (item.Name.EndsWith("Buf"))
                    {
                        if (item.Name.IndexOf("GC") == 0)
                        {
                            gc.Add(item);
                        }
                        else
                        {
                            cg.Add(item);
                        }
                    }
                }
            }
            catch { }

            List<string> funSend = new List<string>();
            for (int i = 0; i < cg.Count; i++)
            {
                Type type = cg[i];
                funSend.AddRange(GetSendList(type, GetPropertyInfoArray(type)));
            }
            Plus.GIO.DirectoryPlus.CreateDirectory("EditorTxt");
            System.IO.File.WriteAllLines("EditorTxt/SendList.txt", funSend);
            
        }

        private static System.Reflection.PropertyInfo[] GetPropertyInfoArray(Type type)
        {
            System.Reflection.PropertyInfo[] props = null;
            try
            {
                object obj = Activator.CreateInstance(type);
                props = type.GetProperties(System.Reflection.BindingFlags.Public | System.Reflection.BindingFlags.Instance);
            }
            catch (Exception ex)
            { }

            return props;
        }

        public static List<string> GetSendList(Type type, System.Reflection.PropertyInfo[] props)
        {
            List<string> fun = new List<string>();

            string typeName = type.Name;

            string top = "public static void Send{0}(string type {1}){";
            string line1 = "\tNetMessageType netMessage = (NetMessageType)Enum.Parse(typeof(NetMessageType), type);";
            string line2 = "\t{0} cg = new {0}();";
            string line3 = "\tnetMessage.PackageAndSend(cg);";
            string end = "}";

            string attStr = ",{0} {1}";
            string att2Str = "\tcg.{0} = {0};";
            string att = "";
            List<string> att2 = new List<string>();

            for (int i = 0; i < props.Length; i++)
            {
                string typrStr = props[i].PropertyType.Name;
                string attName = props[i].Name;

                att += string.Format(attStr, typrStr, attName);
                att2.Add(string.Format(att2Str, props[i].Name));
            }

            top = top.Replace("{0}", typeName);
            top = top.Replace("{1}", att);
            fun.Add(top);
            fun.Add(line1);
            fun.Add(string.Format(line2, typeName));
            fun.AddRange(att2);
            fun.Add(line3);
            fun.Add(end);

            return fun;


        }
        public static List<string> GetReceiveList(Type type, System.Reflection.PropertyInfo[] props)
        {
            List<string> fun = new List<string>();
            string typeName = type.Name;

            string top = "public static StringPlusData Get{0}Data(){";
            string end = "}";

            return fun;
        }

        public static void GetNetMessageType()
        {

            string[] names = System.Enum.GetNames(typeof(NetMessageType));
            int[] values = (int[])System.Enum.GetValues(typeof(NetMessageType));

            string attStr = "\t{0} = \"{0}\",";
            string attStr2 = "\t{0}_int = {1},";
            List<string> atts = new List<string>();
            atts.Add("LuaNetMessageType={");
            for (int i = 0; i < names.Length; i++)
            {
                atts.Add(string.Format(attStr, names[i]));
            }
            for (int i = 0; i < values.Length; i++)
            {
                atts.Add(string.Format(attStr2, names[i],values[i]));
            }
            atts.Add("}");

            Plus.GIO.DirectoryPlus.CreateDirectory("EditorTxt");
            System.IO.File.WriteAllLines("EditorTxt/NetMessageType.txt", atts);
        }

    }
}
