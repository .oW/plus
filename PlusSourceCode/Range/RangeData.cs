﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Game.Plus.GRange
{
    public class RangeData<T>
    {
        protected T max, now, c;

        protected T Max { get => max; set => max = value; }
        protected T Now { get => now; set => now = value; }
        protected T C { get => c; set => c = value; }

        public T GetNow()
        {
            return now;
        }
        public void GetRangeByData(T max,T now,float time)
        {
            Max = max;
            Now = now;
            C = GetRange(now,max,time);
        }
        protected virtual T GetRange(T now, T max, float time)
        {
            return default(T);
        }
        public virtual int ChangeRange()
        {
            return 0;
        }
        /*
        public static D GetData<D>()
            where D : RangeData<T>,new()
        {
            return new D();
        }*/
    }
}
