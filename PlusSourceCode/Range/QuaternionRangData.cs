﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;

namespace Game.Plus.GRange
{
    public class QuaternionRangData : RangeData<Quaternion>
    {
        protected override Quaternion GetRange(Quaternion now, Quaternion max, float time)
        {
            return QuaternionPlus.GetRange(now, max, time);
        }
        public override int ChangeRange()
        {
            int flag = 0;
            flag += QuaternionPlus.ChangeRange(ref now, Max, C);
            return flag;
        }
    }
}
