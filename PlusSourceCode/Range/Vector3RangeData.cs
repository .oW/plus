﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;

namespace Game.Plus.GRange
{
    public class Vector3RangeData : RangeData<Vector3>
    {
        protected override Vector3 GetRange(Vector3 now, Vector3 max, float time)
        {
            return Vector3Plus.GetRange(now, max, time);
        }
        public override int ChangeRange()
        {
            int flag = 0;
            flag += Vector3Plus.ChangeRange(ref now, Max, C);
            return flag;
        }
    }
}
