﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;

namespace Game.Plus.GRange
{
    public class Vector2RangeData : RangeData<Vector2>
    {
        public override int ChangeRange()
        {
            return Vector2Plus.ChangeRange(ref now, max, c);
        }
        protected override Vector2 GetRange(Vector2 now, Vector2 max, float time)
        {
            return Vector2Plus.GetRange(now, max, time);
        }
    }
}
