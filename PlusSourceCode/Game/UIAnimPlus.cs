﻿using Game.Plus;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;
using UnityEngine.UI;
using UnityEditor;

namespace Game.Plus.Game
{
    public static class UIAnimPlus
    {
        #region 封装的动画

        /// <summary>
        /// 上下抖动
        /// </summary>
        /// <param name="animObject"></param>
        /// <param name="time"></param>
        /// <param name="_from"></param>
        /// <param name="_to"></param>
        /// <param name="repeatCount"></param>
        /// <param name="delayTime"></param>
        /// <param name="interp"></param>
        /// <param name="IsIgnoreTimeScale"></param>
        /// <param name="repeatType"></param>
        /// <param name="callBack"></param>
        /// <param name="parameter"></param>
        public static void UIAnimPingPangY<C>(
            this C c,
            float time,
            float _from,
            float _to,
            int repeatCount = 2,
            //暂时无需更改
            float delayTime = 0,
            InterpType interp = InterpType.Default,
            bool IsIgnoreTimeScale = false,
            RepeatType repeatType = RepeatType.PingPang,
            AnimCallBack callBack = null,
            object[] parameter = null
            )where C : Component
        {
            Vector3 v3 = c.gameObject.asC<RectTransform>().localPosition;
            Vector3 from = v3;
            Vector3 to = v3;
            time /= 10;
            from.y += _from;
            to.y -= _to;
            AnimSystem.UguiMove(c.gameObject, from, to, time, delayTime, interp, IsIgnoreTimeScale, repeatType, repeatCount, (objs) => {
                c.UIAnimMove(v3, 0.05f);
            }, parameter);
        }

        public static void UIAnimPingPangY<C>(
            this C c,
            float time,
            int repeatCount = 2,
            //暂时无需更改
            float delayTime = 0,
            InterpType interp = InterpType.Default,
            bool IsIgnoreTimeScale = false,
            RepeatType repeatType = RepeatType.PingPang,
            AnimCallBack callBack = null,
            object[] parameter = null
            ) where C : Component
        {
            Vector3 v3 = c.gameObject.asC<RectTransform>().localPosition;
            Vector3 from = v3;
            Vector3 to = v3;
            time /= 10;
            from.y += 50;
            to.y -= 50;
            AnimSystem.UguiMove(c.gameObject, from, to, time, delayTime, interp, IsIgnoreTimeScale, repeatType, repeatCount, (objs) => {
                c.UIAnimMove(v3, 0.05f);
            }, parameter);
        }


        /// <summary>
        /// 左右抖动
        /// </summary>
        /// <param name="c"></param>
        /// <param name="time"></param>
        /// <param name="_from"></param>
        /// <param name="_to"></param>
        /// <param name="repeatCount"></param>
        /// <param name="delayTime"></param>
        /// <param name="interp"></param>
        /// <param name="IsIgnoreTimeScale"></param>
        /// <param name="repeatType"></param>
        /// <param name="callBack"></param>
        /// <param name="parameter"></param>
        public static void UIAnimPingPangX<C>(
            this C c,
            float time,
            float _from,
            float _to,
            int repeatCount = 4,
            //暂时无需更改
            float delayTime = 0,
            InterpType interp = InterpType.Default,
            bool IsIgnoreTimeScale = false,
            RepeatType repeatType = RepeatType.PingPang,
            AnimCallBack callBack = null,
            object[] parameter = null
            )where C : Component
        {
            Vector3 v3 = c.gameObject.asC<RectTransform>().localPosition;
            Vector3 from = v3;
            Vector3 to = v3;
            time /= 20;
            from.x += _from;
            to.x -= _to;

            AnimSystem.UguiMove(c.gameObject, from, to, time, delayTime, interp, IsIgnoreTimeScale, repeatType, repeatCount, (objs) => {
                c.UIAnimMove(v3, 0.05f);
            }, parameter);
        }

        public static void UIAnimPingPangX<C>(
            this C c,
            float time,
            int repeatCount = 4,
            //暂时无需更改
            float delayTime = 0,
            InterpType interp = InterpType.Default,
            bool IsIgnoreTimeScale = false,
            RepeatType repeatType = RepeatType.PingPang,
            AnimCallBack callBack = null,
            object[] parameter = null
            ) where C : Component
        {
            Vector3 v3 = c.gameObject.asC<RectTransform>().localPosition;
            Vector3 from = v3;
            Vector3 to = v3;
            time /= 20;
            from.x += 50;
            to.x -= 50;

            AnimSystem.UguiMove(c.gameObject, from, to, time, delayTime, interp, IsIgnoreTimeScale, repeatType, repeatCount, (objs) => {
                c.UIAnimMove(v3, 0.05f);
            }, parameter);
        }

        /// <summary>
        /// 大小动画
        /// </summary>
        /// <typeparam name="C"></typeparam>
        /// <param name="c">组件</param>
        /// <param name="v2SizeNow">初始大小</param>
        /// <param name="multiple">缩放倍数</param>
        /// <param name="time">时间</param>
        public static void UIAnimSize<C>(
            this C c,
            float multiple,
            float time
            )where C : Component
        {
            Vector2 v2SizeNow = c.GetComponent<RectTransform>().sizeDelta;
            AnimSystem.UguiSizeDelta(c.gameObject, null, v2SizeNow * multiple, time);
        }

        public static void UIAnimSize<C>(
           this C c,
           Vector2 v2SizeNow,
           float time,
        AnimCallBack callBack = null,
        float delayTime = 0,
        InterpType interp = InterpType.Default,
        bool IsIgnoreTimeScale = false,
        RepeatType repeatType = RepeatType.Once,
        int repeatCount = -1
           ) where C : Component
        {
            AnimSystem.UguiSizeDelta(c.gameObject, null, v2SizeNow, time, delayTime, interp, IsIgnoreTimeScale, repeatType, repeatCount, callBack);
        }

        /// <summary>
        /// 淡入淡出
        /// </summary>
        /// <typeparam name="C"></typeparam>
        /// <param name="c"></param>
        /// <param name="now"></param>
        /// <param name="next"></param>
        /// <param name="time"></param>
        public static void UIAnimAlpha<C>(
            this C c,
            float now,
            float next,
            float time
            )where C : Component
        {
            UIAnimAlpha(c, now, next, time,null);
        }

        /// <summary>
        /// 淡入淡出
        /// </summary>
        /// <typeparam name="C"></typeparam>
        /// <param name="c"></param>
        /// <param name="now"></param>
        /// <param name="next"></param>
        /// <param name="time"></param>
        public static void UIAnimAlpha<C>(
            this C c,
            float now,
            float next,
            float time,
            AnimCallBack action,
            float delayTime = 0,
        InterpType interp = InterpType.Default,
        bool isChild = false,
        bool IsIgnoreTimeScale = false,
        RepeatType repeatType = RepeatType.Once,
        int repeatCount = -1
            ) where C : Component
        {
            AnimSystem.UguiAlpha(c.gameObject, now, next, time, delayTime,interp, isChild, IsIgnoreTimeScale, repeatType,repeatCount,(objs)=> { 
                if (action != null)
                {
                    action();
                }
#if UNITY_EDITOR
                if (c != null)
                {
                    EditorUtility.SetDirty(c);
                }
#endif
            }
            );
        }

        /// <summary>
        /// 移动
        /// </summary>
        /// <typeparam name="C"></typeparam>
        /// <param name="c"></param>
        /// <param name="next"></param>
        /// <param name="time"></param>
        public static void UIAnimMove<C>(
            this C c,
            Vector3 next,
            float time
            )where C : Component
        {
            AnimSystem.UguiMove(c.gameObject, null, next, time);
        }

        public static void UIAnimMove<C>(
          this C c,
          Vector3 next,
          float time,
          AnimCallBack action,
        float delayTime = 0,
        InterpType interp = InterpType.Default,
        bool IsIgnoreTimeScale = false,
        RepeatType repeatType = RepeatType.Once,
        int repeatCount = -1
          ) where C : Component
        {
            AnimSystem.UguiMove(c.gameObject, null, next, time, delayTime, interp, IsIgnoreTimeScale, repeatType, repeatCount, action);
        }

        public static void UIAnimExpression<C>(
            this C c,
            string sprName,
            Vector2 v2,
            Vector2 v2Sc
            )where C : Component
        {
            Image img = null;
            if(c.transform.childCount >= 1)
            {
                img = c.gameObject.GetChild<Image>(0);

                if (sprName == null || sprName == "")
                {
                    if (img != null)
                    {
                        img.gameObject.SetActive(false);
                    }
                    return;
                }
            }
            else
            {
                img = ImagePlus.GetImage();
            }
            if(img != null)
            {
                img.color = Color.white;
                img.gameObject.SetActive(true);
                img.transform.SetParent(c.transform);
                img.SetTexture2DByName(sprName);
                img.name = sprName;
                img.rectTransform.localScale = v2Sc;
                img.rectTransform.localPosition = v2;
                Vector2 v2Size = img.rectTransform.sizeDelta;

                img.UIAnimSize(v2Size * 1.25f, 0.3f, (objs) =>
                {
                    img.UIAnimSize(v2Size, 0.3f);
                });
            }
        }

        public static void UIAnimScale<C>(this C c, Vector3? from, Vector3 to,
        float time = 0.5f, 
        AnimCallBack callBack = null,
        InterpType interp = InterpType.Default,
        bool IsIgnoreTimeScale = false,
        RepeatType repeatType = RepeatType.Once,
        int repeatCount = -1,
        float delayTime = 0,
        object[] parameter = null) where C : Component
        {
            AnimSystem.Scale(c.gameObject, from, to, time, interp, IsIgnoreTimeScale, repeatType, repeatCount, delayTime,callBack);
        }
        #endregion

    }
}
