﻿using Plot.Data;
using Data;
using Game.Plus;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using GameUI.Plot;
using UnityEngine;
using UnityEngine.UI;

namespace Game.Plus.Game
{

    public static class UIImagePlua
    {
        const float width = 1920;
        const float height = 1080;

        /// <summary>
        /// 设置
        /// Image
        /// 的
        /// Sprite
        /// </summary>
        /// <param name="image"></param>
        /// <param name="path">图片在 Resources 下的路径</param>
        public static void SetSpriteRef(ref Image image, string path)
        {
            if (path == null || path == "")
            {
                return;
            }
            Texture2D _tex = ResourceManager.LoadByPath<Texture2D>(path);
            image.sprite = Sprite.Create(_tex, new Rect(0, 0, _tex.width, _tex.height), Vector2.zero);
        }

        /// <summary>
        /// 设置
        /// Image
        /// 的
        /// Sprite
        /// </summary>
        /// <param name="image"></param>
        /// <param name="path">图片在 Resources 下的路径</param>
        public static void SetTexture2DByPath(this Image image, string path)
        {
            if (path == null || path == "")
            {
                return;
            }
            Texture2D _tex = ResourceManager.LoadByPath<Texture2D>(path);
            image.sprite = Sprite.Create(_tex, new Rect(0, 0, _tex.width, _tex.height), Vector2.zero);

        }

        /// <summary>
        /// 设置
        /// Image
        /// 的
        /// Sprite
        /// </summary>
        /// <param name="image"></param>
        /// <param name="path">图片在 Resources 下的路径</param>
        public static void SetTexture2DByName(this Image image, string name)
        {
            if (name == null || name == "" || image == null)
            {
                return;
            }
            Texture2D _tex = ResourceManager.Load<Texture2D>(name);
            image.sprite = Sprite.Create(_tex, new Rect(0, 0, _tex.width, _tex.height), Vector2.zero);
            image.rectTransform.sizeDelta = new Vector2(_tex.width, _tex.height);
        }

        /// <summary>
        /// 设置
        /// Image
        /// 的
        /// Sprite
        /// </summary>
        /// <param name="image"></param>
        /// <param name="path">图片在 Resources 下的路径</param>
        public static void SetTexture2D(this Image image, Texture2D _tex)
        {
            if (_tex == null || image == null)
            {
                return;
            }
            image.sprite = Sprite.Create(_tex, new Rect(0, 0, _tex.width, _tex.height), Vector2.zero);
            image.rectTransform.sizeDelta = new Vector2(_tex.width, _tex.height);
        }
        public static void SetSpriteByPath(this Image image, string path)
        {
            if (path == null || path == "")
            {
                return;
            }
            Sprite sprite = ResourceManager.LoadByPath<Sprite>(path);
            image.sprite = sprite;

        }

        public static void SetSpriteByName(this Image image, string name,bool isNull = false)
        {
            if (image == null)
            {
                return;
            }
            if(name == null || name == "")
            {
                if (isNull)
                {
                    image.sprite = null;
                }
                return;
            }
            Sprite sprite = ResourceManager.Load<Sprite>(name);
            image.sprite = sprite;
        }
        
    }
}
